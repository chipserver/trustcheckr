<?php
namespace App\Repository\Lib;
use App\Http\Controllers\Controller;
use App\MasterEmail;
use DB; 
class EmailSearch_lib extends Controller
{
     public static function isset_func($data){
         return isset($data)?$data:"";
     }

    public static function emailsearchlib_func($data){
         $response['status']['code']=0;
         $response['status']['message']="Users details found.";
         $response['status']['data']=array();
        //$data['email']="mradhip@rediff.com";
         try {
         
           $zerobounceAPiKey= env('ZerobounceAPiKey');
           $zerobounce=file_get_contents('https://api.zerobounce.net/services.asmx/validate?apikey='.$zerobounceAPiKey.'&email='.$data['email']);
           $zerobounce =json_decode($zerobounce, true);
                 
          }catch (\Exception $e) {
             $zerobounce =array();
          }

         try {
         
         $picasaweb=file_get_contents('http://picasaweb.google.com/data/entry/api/user/'.$data['email'].'?alt=json');
         $picasaweb =json_decode($picasaweb, true);
         $googleId = isset($picasaweb['entry']['title']['$t'])?$picasaweb['entry']['title']['$t']:"";
         $googleapis=file_get_contents('https://www.googleapis.com/plus/v1/people/'.$googleId.'?key='.env('Googleapis'));
         $googleapis =json_decode($googleapis, true);

         }catch (\Exception $e) {
            $googleapis=array();
                
         }
         
      
              try {
          $response['status']['data']['email_check'] =$zerobounce;
          $response['status']['data']['googleapis']  =$googleapis;
          

           $master_email =  MasterEmail::firstOrNew(['email' => $data['email']]); 
           $master_email->account  =isset($response['status']['data']['email_check']['account'])?$response['status']['data']['email_check']['account']:"";  
           $master_email->domain  =isset($response['status']['data']['email_check']['domain'])?$response['status']['data']['email_check']['domain']:""; 
           $master_email->firstname  =isset($response['status']['data']['email_check']['firstname'])?$response['status']['data']['email_check']['firstname']:""; 
           $master_email->lastname  =isset($response['status']['data']['email_check']['lastname'])?$response['status']['data']['email_check']['lastname']:""; 
           $master_email->gender  =isset($response['status']['data']['email_check']['gender'])?$response['status']['data']['email_check']['gender']:""; 
           $master_email->kind  =isset($response['status']['data']['googleapis']['kind'])?$response['status']['data']['googleapis']['kind']:""; 
           $master_email->id  =isset($response['status']['data']['googleapis']['id'])?$response['status']['data']['googleapis']['id']:""; 
           $master_email->response =$response['status']['data'];
           $master_email->save();
               
            if(isset($GLOBALS['apikeyID'])){
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
             

           $apikeyID=$GLOBALS['apikeyID'];
           $insert_db=array(
              'apikeyID'=>$apikeyID,
              'requesttime'=>time(),
              'requestIP'=>$ipaddress,
              'response' =>  $response['status']['data'],

               );
       
          
 
           DB::collection('apikeys')->where('_id',$apikeyID)->decrement('remcount');
           DB::collection('apis_analytics')->insertGetId($insert_db);
       
          }
          
               
          }catch (\Exception $e) {
                 $response['status']['code']=2;
                 $response['status']['message']="No data Found.";
                
          }
       
          return  $response;
        
               
    }


   
    
}
