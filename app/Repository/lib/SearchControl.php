<?php
namespace App\Repository\Lib;
use App\Http\Controllers\Controller;
class SearchControl extends Controller
{


    public static function loginfb($url, $header=NULL, $cookie=NULL, $p=NULL){

        $userAgent='Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36';
         
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, $header);
        curl_setopt($ch, CURLOPT_NOBODY, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        curl_setopt($ch, CURLOPT_USERAGENT,$userAgent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        if ($p) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $p);
        }
        $result = curl_exec($ch);
        $urls = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close($ch);
        $GLOBALS['urls'] =  $urls;
        if ($result) {
            return $result;
        } else {
            return curl_error($ch);
        }
        curl_close($ch);
    }


    public static function search_string($string){

		 $search = explode(",",  $string);
              $ocr= $search[0];
              if (strpos($ocr,'faceboolk')!== FALSE ) {
              $ocr_data = explode("faceboolk\n",$ocr);  
              $ocr_data =    $ocr_data [1];
            }else {
                $ocr_data = explode("\n",$ocr);
               // die(json_encode(count($ocr_data) ));
                $ocr_data =    $ocr_data [count($ocr_data)-1];
            }
             
                    

              $patterns = array();
              $patterns[0] = '/ at /';
              $patterns[1] = '/ of /';
              $patterns[2] = '/ is /';
              $patterns[3] = '/recommend/';
              $patterns[4] = '/to a friend/';
              $patterns[5] = '/Interests/';
              $patterns[6] = '/interests/';
              $patterns[7] = '/lnterests/';
              $patterns[8] = '/1nterests/';
              $patterns[9] = '/ :  /';
              $patterns[10] ='/\na/';
              $patterns[11] = '/current location/';
              $patterns[12] = '/Current location/';
              $patterns[13] = '/Current/';
              $patterns[14] = '/Location/';
              $patterns[15] = '/verified by/';
              $patterns[16] = '/verified by photo/';
              $patterns[17] = '/friends/';
              $patterns[18] = '/Instagram/';
              $patterns[19] = '/instagram/';
              $patterns[20] = '/Instagram/';
              $patterns[21] = '/1nstagram/';
              $patterns[22] = '/mile away/';
              $patterns[23] = '/km/';
              $patterns[24] = '/km away/';
              $patterns[25] = '/kms/';
              $patterns[26] = '/kms away/';
                          $patterns[27] = '/kilometer/';
                          $patterns[28] = '/kilometer/';
                        $patterns[29] = '/kilometers/';
                        $patterns[30] = '/kilometer away/';
                        $patterns[31] = '/mutual friend/';
                        $patterns[32] = '/Common/';
                        $patterns[33] = '/common/';
                          $patterns[34]  ='/\nn/';
                          $patterns[35] ='/\n/';
                          $patterns[39] = '/kilometers away/';
                          $patterns[40] = '/Top/';
                          $patterns[41] = '/insta/';
                          $patterns[42] = '/miles away/';
                          $patterns[43] = '/miles/';
                          $patterns[44] = '/mile/';
                          $patterns[44] = '/Mutual Friend/';
                           $patterns[45] = '/Interest/';
                        
                          $patterns[46] = '/active today/';
                          $patterns[47] = '/active/';
                          $patterns[48] = '/Active/';
                            $patterns[49] = '/mutual/';
                         
                          $patterns[50] = '/active today/';
                          $patterns[51] = '/Active today/';
                          $patterns[52] = '/Active today/';
                          $patterns[53] = '/active a/';
                          $patterns[54] = '/Active a/';
                          $patterns[55] = '/Active a/';
                          $patterns[56] = '/Active a/';
                          $patterns[57] = '/less than/';
                          $patterns[58] = '/3 1/';
                          $patterns[59] = '/RECOMMANDEZ/';
                          $patterns[60] = '/Downloads/';
                          $patterns[61] = '/Similar/';
                          $patterns[62] = '/706/';
                          $patterns[63] = '/0 verified/';
                          $patterns[64] = '/O verified/';
                          $patterns[65] = '/verified by/';
                          $patterns[66] = '/2 Lifestyle/';
                          $patterns[67] = '/RECOMANDA/';
                          $patterns[68] = '/3 Similar/';
                          $patterns[69] = '/To see/';
                          $patterns[70] = '/Say Hi/';
                          $patterns[71] = '/A 12/';
                          $patterns[72] = '/2 2/';
                          $patterns[73] = '/7 2/';
                          $patterns[74] = '/FREE/';
                          $patterns[75] = '/45.355/';
                          $patterns[76] = '/4 Lignende/';
                          $patterns[77] = '/4 4,5/';
                          $patterns[78] = '/31 ls/';
                          $patterns[79] = '/31 1s/';
                          $patterns[80] = '/3 2/';
                          $patterns[81] = '/Downloads 2 Similar/';
                          $patterns[82] = '/Downloads 706/';
                          $patterns[83] = '/Downloads/';
                          $patterns[84] = '/Similar/';
                          $patterns[85] = '/RECOMMANDEZ/';
                          $patterns[86] = '/705 Join 25 million/';
                          $patterns[87] = '/4 10/';
                          $patterns[88] = '/3 2/';
                          $patterns[89] = '/0/';
                          $patterns[90] = '/1/';
                          $patterns[91] = '/2/';
                          $patterns[92] = '/5/';
                          $patterns[93] = '/6/';
                          $patterns[94] = '/3/';
                          $patterns[95] = '/7/';
                          $patterns[96] = '/9/';
                          $patterns[97] = '/8/';
                          $patterns[98] = '/=/';
                          $patterns[99] = '/!/';
                          $patterns[100] = '/4/';
                          $patterns[101] = '/ Downloads 706/';
                          $patterns[101] = '/ Downloads 706/';
                          $patterns[101] = '/ Downloads 706/';
                          $patterns[102] = '/ 10 Join 25 million/';
                          $patterns[103] = '/ 708 Lifestyle/';
                          $patterns[104] = '/ Also went/';
                          $patterns[105] = '/ Say Hi/';
                          $patterns[106] = '/I Speak/';
                          $patterns[107] = '/I Speak/';
                          $patterns[108] = '/1 mutual/';
                          $patterns[109] = '/1 Mutual/';
                          $patterns[110] = '/l mutual/';
                          $patterns[111] = '/1 mutual/';
                          $patterns[112] = '/11/';
                          $patterns[113] = '/fn/';
                          $patterns[114] = '/</';
                           $patterns[115] = '/away/';

                          $pure_str=preg_replace($patterns,' ', $search[1]);
                          
                          $pure_str=ltrim($pure_str,' ');

                          $ocr_clg = explode(" ",$pure_str);
                          $ocr_clg = preg_replace('!\s+!', ' ', $ocr_clg);        
              
                  $sec_word=$first_word="";
                  $word=0;
                  for($x=0;$x<count($ocr_clg);$x++) { 
                
                     if((strlen($ocr_clg[$x])>1) && ($word==0)) {      
                           $first_word="%20".$ocr_clg[$x];
                           $word=1;
                         
                      }else if((strlen($ocr_clg[$x])>1) && $word==1) {
                       $sec_word="%20".$ocr_clg[$x];
                       break;
                     }
                     
                  }
                  $ocr_data=str_replace(" ","%20",$ocr_data); 
                  $query=$ocr_data.$first_word.$sec_word;
                   
                  return  $query;
                 
    }
    
    
    
}
