<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use DB;

class  Apiurl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null )
    {

      $headers = apache_request_headers();

      if (isset($headers['apiKey'])) {
         $apiKey= $headers['apiKey'];
         $GLOBALS['apikey']=$apiKey;


           $getkeys= DB::collection('users')->where('key',$apiKey)->get();
            
         if(0== count( $getkeys)) {
              $response['status']['code'] = 1024;
              $response['status']['message'] = "Invalid request.";
              die(json_encode($response ));
         }

          $GLOBALS['apikeyID']= (string)$getkeys[0]['_id'];
          return $next($request);

       }else{
           $response['status']['code'] = 1024;
           $response['status']['message'] = "Invalid request.";
           die(json_encode($response ));
       }
    }
}
