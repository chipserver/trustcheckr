<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
 public function __construct(Guard $guest)
    {
        $this->auth = $guest;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        // die(json_encode(array_keys(config('auth.guards'))));
    // die(json_encode($guard));
  //          $guards = array_keys(config('auth.guards'));
  //   foreach ($guards as $guard) {
  //   if(Auth::guard($guard)->check()) 

  //       die($guard);
  // }
        // $s=a
      
      
        
        if (Auth::guard($guard)->check()) {
            if (Auth::guard($guard)->user()->roll_id==1000) {
                return redirect('/superadmin/dashbord');
            
        }
        else if (Auth::guard($guard)->user()->roll_id==1001) {
                return redirect('/account/user-dashbord');
            
        }
    }
        return $next($request);
    }
}
