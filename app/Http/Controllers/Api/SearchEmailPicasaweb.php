<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB; 
use App\Repository\lib\EmailSearch_lib;
class SearchEmailPicasaweb extends Controller
{
        
    
    public function searchemail(Request $request) {

        $rules = array(
             'email' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        } 
         
         $data = $request->all();
         $res=EmailSearch_lib::emailsearchlib_func($data);
         die(json_encode($res));
    }
    
    
}
