<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class DLControl extends Controller
{
     
    public  static function get_dl(Request $request){
        $rules = array(
             'dob' => 'required' ,
             'dlno' => 'required' 
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        } 
        
        $data = $request->all();
		$post = [
			'dob' =>$data['dob'],
			'dl_no' =>$data['dlno']
	 	];
		
    	$ch = curl_init('https://api.whatsloan.com/v1/ekyc/dlAuthNew');
    	$headers = [
         'token:9290123020b140515cc29bfdb776e3b471af0a43',
        ];
        
       
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$response = curl_exec($ch);
		
		 if(isset($GLOBALS['apikeyID'])){
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
             

           $apikeyID=$GLOBALS['apikeyID'];
           $insert_db=array(
              'apikeyID'=>$apikeyID,
              'requesttime'=>time(),
              'requestIP'=>$ipaddress,
              'response' =>  json_decode($response),

               );
       
          
 
           DB::collection('apikeys')->where('_id',$apikeyID)->decrement('remcount');
           DB::collection('apis_analytics')->insertGetId($insert_db);
       
          }
		$res['status']['code']    = 0;
		$res['status']['message'] = "DL Details";
        $res['data']['dl_info']  = json_decode($response);
        
        die(json_encode($res ));
          
  }      

    
    
   
}
