<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\FBScoreMaster;
use Storage; 
use Aws\Rekognition\RekognitionClient;
 
class FBScoreGenaratControl extends Controller {
   
   
    
     public function genaratescore(Request $request) {
        $inputs    = $request->all();
        $rules     = array(
            'fbid'=>'required|max:500',
            'name' => 'required|max:500',
            'photo' => 'max:5000',
            'gender' =>'max:20',
            'placeofWork' => 'max:500',
            'designation' => 'max:500',
            'placeofStudy' => 'max:500',
            'friendCount'=> 'numeric|digits_between:0,10000',
            'likesCount' => 'numeric|digits_between:0,10000',
            'aboutLine'=> 'max:500',
	      	);

         $validator = Validator::make($request->all(), $rules);
         $data      = $request->all();
         $leastpagescoring=$listfriend=$leastpagesrating=0;
         if ($validator->fails()) {
         $response['status']['code']=1;
           $res = $validator->getMessageBag()->toArray();
           foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
                 return response()->json($response);
            }
            
        }
          $score=0;
          $WFL=2; $WLI=1; $WPHFA=6; $WPGGE=2; $WNGGE=1; $WWO=1; $WST=1;
          $SFL=$SLI=$NGGE=$PHFA=$PGGE=$SWO=$SST=0;
          
          $listfriendCounts=0;
          if(array_key_exists('friendCount',$data)){
            if($data['friendCount']>500){
               $SFL=100;
            }else if($data['friendCount'] <= 500 && $data['friendCount']>100 ){
               $SFL=90;
            }else if($data['friendCount'] <= 100 && $data['friendCount']>50 ){
  			       $SFL=50;
  		      }else if($data['friendCount'] <= 50 && $data['friendCount']>20 ){
               $SFL=10;
               $listfriendCounts=1;
            }
          }
          
          if(array_key_exists('likesCount',$data)){ 
              if($data['likesCount']>50){
                 $SLI=100;
              }else if($data['likesCount'] <= 50 && $data['likesCount']>10 ){
                 $SLI=90;
              }else if($data['likesCount'] <= 10 && $data['likesCount']>5 ){
                 $SLI=50;
              }else if($data['likesCount'] <= 5 && $data['likesCount']>1 ){
                 $SLI=10;
              }
           }


           $arrContextOptions=array(
				"ssl"=>array(
					"verify_peer"=>false,
					"verify_peer_name"=>false,
				),
			);
		   $result = file_get_contents("https://api.genderize.io/?name=".$data['name'], false, stream_context_create($arrContextOptions));
           //$result=file_get_contents('https://api.genderize.io/?name='.$data['name']);
           $result =json_decode($result, true);
           
           if(array_key_exists('gender',$data)){
           if(strtolower($result['gender'])==strtolower($data['gender'])) {
              $NGGE=100;
            }}

           
         
           if(array_key_exists('placeofWork',$data))  $SWO=100;
           if(array_key_exists('placeofStudy',$data)) $SST=100;
          

          
          
		
	   if(array_key_exists('photo',$data)){ 
		if($data['photo']!=""){
		if(@getimagesize($data['photo'])){
		$image_base64 = base64_encode(file_get_contents($data['photo']));
		$res['requests'] = array();
        $res['requests']['image']['content'] = $image_base64;
        $res['requests']['features'] = array();
        $res['requests']['features'][0]['type'] = "TEXT_DETECTION";
        $res['requests']['features'][1]['type'] = "SAFE_SEARCH_DETECTION";
        $res['requests']['features'][2]['type'] = "WEB_DETECTION";
        $data_string = json_encode($res);
        $visionAPiAuth= env('VisionAPiAuth');      
        $curl = curl_init('https://vision.googleapis.com/v1/images:annotate?key='.$visionAPiAuth);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        $vresult = $result;
        
        if (isset($result['error'])) {
            $info['status']['code'] = 2;
            $info['status']['message'] = "Invalid base64 image.";
            die(json_encode($info));
        }
        if (isset($result['responses'][0]['error'])) {
            $response['status']['code'] = 3;
            $response['status']['message'] = "Bad image data.";
            return response()->json($response);
        }
            if($data['photo']=="")
            $score=$score+10;
		}}
		
		$data['photo']=base64_encode(file_get_contents($data['photo']));    
        $image = base64_decode($data['photo']);
        //  die(json_encode("sad"));    
        $img1 = time().'.jpg';
        $t = Storage::disk('s3')->put($img1,$image, 'public');
        $source_image = Storage::disk('s3')->url($img1);
        // die(json_encode($source_image));
             try {
       
            $client = RekognitionClient::factory(array(    
                         'credentials' => array(       
                         'key'    => env('S3_KEY'),
                         'secret' =>  env('S3_SECRET')),  
                         'region' =>  env('S3_REGION'),  
                         'version'=> 'latest',   
                      ));
          
        $faceresult = $client->detectFaces([
            'Attributes' => ['ALL'],
            'Image' => [ // REQUIRED
            'Bytes' => file_get_contents($source_image),
        ],
       
       ]);  
        } catch (\Exception $e) {
                              $matching="";
                   }
		 
		
		}
		 $face['Gender']=isset($faceresult['FaceDetails'][0]['Gender']['Value'])?$faceresult['FaceDetails'][0]['Gender']['Value']:array();
		 
		  if(strtolower($face['Gender']) == strtolower($data['gender'])){
			   $PGGE=100;
		  }
          
          $PHFA=100;
          if (isset($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'])) {
            if (strpos($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'][0]['url'], 'linkedin') !== true) {
               $PHFA=0;
            }  
          }
         //ie(json_encode($vresult));
          
          
           // $SFL=100; $SLI=40; $PHFA=0; $PGGE=100;  $NGGE=100; $SWO=0; $SST=0;
           $div_score= ($WFL*$SFL + $WLI*$SLI + $WPHFA*$PHFA + $WPGGE*$PGGE + $WNGGE*$NGGE + $WWO*$SWO + $WST*$SST);  
           
           $divided_by =($WFL + $WLI + $WPHFA + $WPGGE + $WNGGE + $WWO + $WST);
           
           $finalscore=$div_score/$divided_by;

          
             
          $facebook =  FBScoreMaster::firstOrNew(['fbid' => $data['fbid']]); 
          $facebook->fbid= $data['fbid']; 
          $facebook->name= $data['name'];  
          $facebook->photo= isset($data['photo'])?$data['photo']:"";
          $facebook->placeofWork= isset($data['placeofWork'])?$data['placeofWork']:"";
          $facebook->designation= isset($data['designation'])?$data['designation']:"";
          $facebook->placeofStudy= isset($data['placeofStudy'])?$data['placeofStudy']:"";
          $facebook->friendCount= isset($data['friendCount'])?$data['friendCount']:"";
          $facebook->aboutLine= isset($data['aboutLine'])?$data['aboutLine']:"";
          $facebook->save();
           
         $response['status']['code']=0;
         $response['status']['message']="Trust Score is";
         $response['data']['score']= round($finalscore,2);
         $response['data']['leastfriend']=  $listfriend;
         $response['data']['leastpagescoring']  =  $leastpagescoring;
         $response['data']['leastpagesrating']  =  $leastpagesrating;
         die(json_encode(  $response));
	}
}
