<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use App\Repository\Lib\CurlControl;
use Session;
class WhatsappControl extends Controller {
    
  
    public function get_rateapi(Request $request) {
        $inputs = $request->all();
        $rules = array('image_type' => 'required','image_info' => 'required');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $response['status']['message'] = "Required param missing.";
            return response()->json($response);
        }
        $data = $request->all();
        $userId ="";
        if (isset($data['userId'])) {
            $userId = trim($data['userId']);
        }
        if ($data['image_type'] == 1) {
            $data['image_base64'] = base64_encode(file_get_contents($data['image_info']));
              // die(json_encode(   $data['image_base64']));
        $data['image_base64'] = $data['image_base64'];
        $res['requests'] = array();
        $res['requests']['image']['content'] = $data['image_base64'];
        $res['requests']['features'] = array();
        $inf['type'] = "TEXT_DETECTION";
        array_push($res['requests']['features'], $inf);
        $data_string = json_encode($res);
        $curl = curl_init('https://vision.googleapis.com/v1/images:annotate?key=AIzaSyDvciItJdQ8qyLETBNc4fGWjU_-6r5YWzY');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        if (isset($result['error'])) {
            $info['status']['code'] = 2;
            $info['status']['message'] = "Invalid base64 image.";
            die(json_encode($info));
        }
        if (isset($result['responses'][0]['error'])) {
            $response['status']['code'] = 3;
            $response['status']['message'] = "Bad image data.";
            return response()->json($response);
        }

        $ocr_actual = $result['responses'][0]['fullTextAnnotation']['text'];
        $search = explode("\n", $ocr_actual);
              $line1 =  $search[0];
              $line2 =  $search[1];
              $line3 =  $search[2];
       
        } else { $line1 = $data['image_info'];
              
              $line2 =  "";
              $line3 =  ""; $ocr_actual=$line1;
          }
             
      
            if (DB::collection('post_collection')->where('line1', $line1)->where('line2', $line2)->where('line3', $line3)->count()  == 1) {

                $image_data = DB::collection('post_collection')->where('line1', $line1)->where('line2',$line2)->where('line3', $line3)->get();
              
                $total = 0;
                $rate = 0; // die(json_encode(  $res));
                foreach ($image_data[0]['rating'] as $value) {
                    $total++;
                    $rate = $rate + $value['rate'];
                }
                $rating = $rate / $total;
                $id=(string)$image_data[0]['_id'];
                $line1 = $image_data[0]['line1'];
                $line2 = $image_data[0]['line2'];
                $line3 = $image_data[0]['line3'];
                $ratings = $image_data[0]['rating'];
                $response['status']['code'] = 5;
                $response['status']['message'] = "String match found.";
                } else {

                $info1['data'] = array();
                $comments=array();
                $rating=array();
                $time =time();
                

                 $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';

                 //$user_prefix="user";
                 
                 // die(json_encode(++$userId ));
                  
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);

                  }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];
                 // die(json_encode( $userName));
                  $id=DB::collection('post_collection')->insertGetId(['userName' => $userName,'line1' => $line1, 'line2' => $line2,'line3' => $line3,'rating' => $rating,'comments' =>$comments,'string' => $ocr_actual  ,'added_at' => $time ,'update_at' => $time ]);

                $ratings = array();
              //  $rating =  $info['rate'];
                $total = 1; //die(json_encode(  $rating ));
                $response['status']['code'] = 0;
                $response['status']['message'] = "String inserted to database.";
              
            }
        
            $response['data']['rating'] = round($rating, 2);
            $response['data']['textId'] = (string)$id;
            $response['data']['line1'] =  $line1;
            $response['data']['line2'] =  $line2;
            $response['data']['line3'] =  $line3;
            $response['data']['userId'] = $userId;
            $response['data']['ratings'] = $ratings;
            $response['data']['total_ratings'] = count($ratings)==0?1: count($ratings);
            $_SESSION["response_code"]   = $response['status']['code'];
            $_SESSION["response_textId"] = $response['status']['code'];
           
            // $request->session()->put('response_textId', $response['data']['textId']);
            //  Session::put('response_code',$response['status']['code']);
            // Session::put('response_textId',$response['data']['textId']);
            die(json_encode($response ));
      
    }
    public function update_rateapi(Request $request) {
        $inputs = $request->all();
       // die(jsadasd_sadsad)l
        $rules = array('textId' => 'required', 'rating' => 'numeric|min:0|max:1',);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  $response['status']['message']=$value[0];}
            return response()->json($response);
            
            
        }
        try {
            $data = $request->all();
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';

            $data['userId']= $ipaddress;            
            if (isset($data['userId'])) {
                $userId = trim($data['userId']);
                if ($userId != "") {
                    $exist_rating = DB::collection('post_collection')->where('_id', $data['textId'])->where('rating.userId', $userId)->count();
                    //die(json_encode(   $exist_rating ));
                    if ($exist_rating >= 1) {
                        $response['status']['code'] = 2;
                        $response['status']['message'] = "You have already rated this...";
                        die(json_encode($response));
                    }
                }
            }
            $info1['data'] = array();
            $time = time();
            $checkid=isset($_SESSION['userId'])?$_SESSION['userId']:""; 
            if($checkid!=$data['textId']) {
               
            $_SESSION['userId']= $data['textId'];
            }else { 
              $response['status']['code'] = 5;
              $response['status']['message'] = "You have already rated.";
              die(json_encode($response ));

            }
            //die(json_encode(  $userId ));

             
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);

                  }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId'];  
          
            $info['userId']   = $ipaddress;
            $info['userName'] = $userName;
            $info['rate'] = (int)$data['rating'];
            $info['userType'] = 0;
            $info['ratedat'] = $time;
            array_push($info1['data'], $info);
            $get = DB::collection('post_collection')->where('_id', $data['textId'])->push('rating', $info1['data']);
            $image_data = DB::collection('post_collection')->where('_id', $data['textId'])->get();
         
            //  die(json_encode(  $image_data));
            $total = 0;
            $rate = 0;
            foreach ($image_data[0]['rating'] as $value) {
                $total++;
                $rate = $rate + $value['rate'];
            }
            $rating = $rate / $total;
            $ratings = $image_data[0]['rating'];
            $response['status']['code'] = 0;
            $response['status']['message'] = "Your rating is updated.";
            $response['data']['rating'] = round($rating, 2);
            $response['data']['textId'] = $data['textId'];
            $response['data']['line1'] =  $image_data[0]['line1'];
            $response['data']['line2'] =  $image_data[0]['line2'];
            $response['data']['line3'] =  $image_data[0]['line3'];
            $response['data']['userId'] = $userId;
            $response['data']['ratings'] = $ratings;
            $response['data']['total_ratings'] = count($image_data[0]['rating']);
        }
        catch(\Exception $e) {
            $response['status']['code'] = 1;
            $response['status']['message'] = "Invalid text Id try again.";
        }
        die(json_encode($response));
    }




     public function update_comments(Request $request) {
        $inputs = $request->all();
        $rules = array('textId' => 'required', 'comment' => 'required',);
        $validator = Validator::make($request->all(), $rules);
         if ($validator->fails()) {
            $response['status']['code'] = 1;
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  $response['status']['message']=$value[0];}
            return response()->json($response);
         }

        try {
            $data = $request->all();
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';

            $data['userId']= $ipaddress;            
            if (isset($data['userId'])) {
                $userId = trim($data['userId']);
                if ($userId != "") {
                    $exist_rating = DB::collection('post_collection')->where('_id', $data['textId'])->where('comment.userId', $userId)->count();
                    //die(json_encode(   $exist_rating ));
                    if ($exist_rating >= 1) {
                        $response['status']['code'] = 2;
                        $response['status']['message'] = "You have already commented ...";
                        die(json_encode($response));
                    }
                }
            }
            $info1['data'] = array();
            $time = time();
            
              
                  $count=DB::collection('ip_collections')->where('ip',$ipaddress)->count();

                  if($count==0){

                    $userId =DB::collection('ip_collections')->max('userId');
                    $userId =$userId==null?1:$userId;   
                    DB::collection('ip_collections')->insertGetId(['ip' =>$ipaddress,'userId'=>++$userId ,'added_at' =>$time,'update_at' =>$time]);

                  }

                  $userName=DB::collection('ip_collections')->where('ip',$ipaddress)->get();
                  $userName='user'.$userName[0]['userId']; 
          
            $info['userId']   = $ipaddress;
            $info['userName'] = $userName;
            $info['comment']  = $data['comment'];
            $info['userType'] = 1;
            $info['commentat'] = $time;
             $data['userName']=$userName;

            array_push($info1['data'], $info);
            $get = DB::collection('post_collection')->where('_id', $data['textId'])->push('comments', $info1['data']);
           
            $response['status']['code'] = 0;
            $response['status']['message'] = "Your comment is updated.";
            $response['data']=$data;
        }
        catch(\Exception $e) {
            $response['status']['code'] = 1;
            $response['status']['message'] = "Invalid text Id try again.";
        }
        die(json_encode($response));
    }
  
}
