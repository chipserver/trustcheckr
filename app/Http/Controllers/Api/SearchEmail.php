<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Repository\Lib\SearchControl;
class SearchEmail extends Controller
{
        
    
    public function searchemail(Request $request) {
  
        $rules = array(
             'email' => 'required',
             'image_type' => 'required',
             'image_info' => 'required',
            
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        } 
        $data = $request->all();
        if ($data['image_type'] == 1) { 
            $data['image_base64'] = base64_encode(file_get_contents($data['image_info']));
        } else $data['image_base64'] = $data['image_info'];
    
        
        // vision api search comparision

        $res['requests'] = array();
        $res['requests']['image']['content'] = $data['image_base64'];
        $res['requests']['features'] = array();
        $res['requests']['features'][0]['type'] = "TEXT_DETECTION";
        $res['requests']['features'][1]['type'] = "SAFE_SEARCH_DETECTION";
        $res['requests']['features'][2]['type'] = "WEB_DETECTION";
        $data_string = json_encode($res);
        $visionAPiAuth= env('VisionAPiAuth');      
        $curl = curl_init('https://vision.googleapis.com/v1/images:annotate?key='.$visionAPiAuth);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        $vresult = $result;
      
        if (isset($result['error'])) {
            $info['status']['code'] = 2;
            $info['status']['message'] = "Invalid base64 image.";
            die(json_encode($info));
        }
        if (isset($result['responses'][0]['error'])) {
            $response['status']['code'] = 3;
            $response['status']['message'] = "Bad image data.";
            return response()->json($response);
        }
     
         $safeVision=$result['responses'][0]['safeSearchAnnotation'];

   
        $curl = curl_init('https://api.fullcontact.com/v2/person.json?email='.$data['email']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST,"GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $fullcontactAPiKey= env('FullcontactAPiKey');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('X-FullContact-APIKey:'.$fullcontactAPiKey));
        $result = curl_exec($curl);
        $result_email = json_decode($result,true);
       // die(json_encode($result_email));
        $zerobounceAPiKey= env('ZerobounceAPiKey');
        $curl = curl_init('https://api.zerobounce.net/services.asmx/validate?apikey='.$zerobounceAPiKey.'&email='.$data['email']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST,"GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $bounce = json_decode($result,true);
      
          $bouncename= $bounce['firstname']." ". $bounce['lastname'];
       
          $res=array();
         
 
           $fakePhoto=false;
            
           $token="EAAV8oToZBJGABADlD0GMcjeNgVHt2B7aPmGG03Vbg0Nqi0TBBt4uHVM023S37a0P17LnWDApZAR7hFviZCPBfKxpb7ruk5EfK0KZAgi7Aw2QVidLOa1YlzYsUoimG2NqgOLhnMgm5xPp5OtCbhJPQ2TOz5KoKdapZCueRw2gZBQUZB3JZBvITK1oKhxUkohEymcZD";
       
          if (isset($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'])) {
          
            if (strpos($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'][0]['url'], 'linkedin') !== true) {
                $fakePhoto=true;
            }  
          }
          
          
     
         $cookie=DB::collection('facebook_cookie')->first();   
         $cookie=$cookie['cookie'];  
      
        
        /*
        NOW TO JUST OPEN ANOTHER URL EDIT THE FIRST ARGUMENT OF THE FOLLOWING FUNCTION.
        TO SEND SOME DATA EDIT THE LAST ARGUMENT.
        */

        $html= SearchControl::loginfb("https://www.facebook.com/search/people/?q=".$data['email'],null,$cookie,null);
        
        $fbscrapes=array();
        
        $fbfound=false;
        $id=explode('data-bt="&#123;&quot;id&quot;:',$html);
        
      
        $foundid="";
        if(isset( $id[1])) { 
        $fbfound=true;
          $id=explode(",&quot;",$id[1]);
          $fbscrapes['id']=$id[0];
          $foundid=   $fbscrapes['id'];
          $fb['searching_url'] = 'https://graph.facebook.com/'.$fbscrapes['id'].'?fields=name&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }

        $name = json_decode($res, TRUE);  
        $fbscrapes['name']= $name['name'];
        $profileurl='https://www.facebook.com/'.$id[0];
        $profileimage='https://graph.facebook.com/'.$id[0].'/picture?type=large';
        $fbscrapes['profileimage']=$profileimage;
        
      }
 
        
        $search =array();
        if(!$fbfound){
            $search_name = str_replace(" ", "%20", $bouncename); 
            $fb['searching_url'] = 'https://graph.facebook.com/v2.9/search?fields=name,id,picture.height(320)&limit=25&offset=0&type=user&q=' . $search_name . '&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }
             $search = json_decode($res, TRUE);  
             if (isset($search['data'])) {
         if(count($search['data'])>0) {
             $res=$search['data'];
             $foundid=isset($search['data'][0]['id'])?$search['data'][0]['id']:$search['data']['id'];
             
         } 
            }
         }
        
     
        
        $tagged_photos=$liked_pictures=$commented_pictures=$tagged_posts=$commented_posts=$liked_posts=$liked_pages=$visited_places=0;
        $count=0;
        $scrapingdata=array();
        if($foundid!="") { 
           if(!$fbfound){
           SearchControl::loginfb("https://facebook.com/".$foundid,null,$cookie,null);
                 $urseid=explode("https://www.facebook.com/",$GLOBALS['urls']);
                 $fb_ids = @file_get_contents("https://www.7xter.com/ads/findmyid/index.php?username=" .  $urseid[1]);
                 $foundid = preg_replace('/Your Facebook Numerical ID is /', '', $fb_ids); 
          }
           
        /*
        NOW TO JUST OPEN ANOTHER URL EDIT THE FIRST ARGUMENT OF THE FOLLOWING FUNCTION.
        TO SEND SOME DATA EDIT THE LAST ARGUMENT.
        */
        
     
        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-of/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false   ) {
           $tagged_photos=1;        $count=$count+10;
          
         } 
       
        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-of/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $liked_pictures=1;  $count=$count+10;
        } 


        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-commented/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $commented_pictures=1;  $count=$count+10;
        } 



        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-tagged/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false  ) {
           $tagged_posts=1;   $count=$count+10;
        } 

       

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-commented/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false  ) {
          $commented_posts=1;    $count=$count+10; 
         
        } 


        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-liked/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $liked_posts=1;
        } 

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/pages-liked/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false) {
           $liked_pages=1;  $count=$count+10;
        }  

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/places-visited/",null,$cookie,null);
        if (strpos($html, "aboutInfo") !== false) {
           $visited_places=1;  $count=$count+10;
        }   
          
       $scrapingdata['id']=$foundid;  
       $scrapingdata['tagged_photos']=$tagged_photos;
       $scrapingdata['liked_pictures']=$liked_pictures;
       $scrapingdata['commented_pictures']=$commented_pictures;
       $scrapingdata['tagged_posts']=$tagged_posts;
       $scrapingdata['commented_posts']=$commented_posts;
       $scrapingdata['liked_posts']=$liked_posts;
       $scrapingdata['liked_pages']=$liked_pages;
       $scrapingdata['visited_places']=$visited_places;
       }  
       $err['err_code']=500;
       $err['err_message']="Data Not available";
       $result_email=$result_email['status']==200?$result_email:$err;
        $insert=array( 'bounce'=>$bounce,
                      'email_search'=>$result_email,
                      'safeVision'=>array($safeVision),
                      'fakePhoto'=> $fakePhoto,
                      'fbfound'=>$fbfound,
                      'fbdatainfo'=>array($fbscrapes),
                      'fbsearch'=>  $search, 
                      'datainfoscore'=>$count
                      );

         if(isset($GLOBALS['apikeyID'])){

                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if(isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if(isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
          

           $apikeyID=$GLOBALS['apikeyID'];
           $insert_db=array(
              'apikeyID'=>$apikeyID,
              'requesttime'=>time(),
              'requestIP'=>$ipaddress,
              'response' =>$insert,

               );
       
          // die(json_encode($apikeyID ));
           DB::collection('apikeys')->where('_id',$apikeyID)->decrement('remcount');
           DB::collection('apis_analytics')->insertGetId($insert_db);

         }
        //  $res=  DB::collection('score_response')->insertGetId($insert);
        $response['status']['code'] = 0;
        $response['status']['message'] = "list of user data.";

       
      
      
        $response['data']=$insert;
      
        
        die(json_encode( $response));




    }
    
    

   
}
