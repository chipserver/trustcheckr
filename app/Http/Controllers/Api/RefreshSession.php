<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;

class RefreshSession extends Controller
{
     
    public  static function logattm($url, $header=NULL, $cookie=NULL, $p=NULL)
{
  
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, $header);
    curl_setopt($ch, CURLOPT_NOBODY, $header);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    if ($p) {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $p);
    }
    $result = curl_exec($ch);
    if ($result) {
        return $result;
    } else {
        return curl_error($ch);
    }
    curl_close($ch);
}      

    public static function genarate_sesion() {

      $res=DB::collection('facebook_cookie')->first();   
      
     if(strtotime($res['update_at']) > strtotime("-30 minutes")) {      
		  $EMAIL      =  env('FB_UserName');
		  $PASSWORD   =  env('FB_Password');
		  $cookie="";
		  $a = self::logattm("https://login.facebook.com/login.php?login_attempt=1",true,null,"email=$EMAIL&pass=$PASSWORD");
		  preg_match('%Set-Cookie: ([^;]+);%',$a,$b);
		  $c = self::logattm("https://login.facebook.com/login.php?login_attempt=1",true,$b[1],"email=$EMAIL&pass=$PASSWORD");
		  preg_match_all('%Set-Cookie: ([^;]+);%',$c,$d);
		  for($i=0;$i<count($d[0]);$i++)
		  $cookie.=$d[1][$i].";";
		  DB::collection('facebook_cookie')->delete();
		  DB::collection('facebook_cookie')->insertGetId(['cookie' =>$cookie,'update_at' =>time()]);
     
    }
       

    }
    
    
   
}
