<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use App\Repository\Lib\SearchControl;

class TrustScore extends Controller
{
    
    
    public function genaratescore(Request $request)
    {
        
        $rules     = array(
            'phone_num' => 'required',
            'email' => 'required',
            'name' => 'required'
            
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $res = $validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {
                $response['status']['message'] = $value[0];
            }
            $response['status']['message'] = $value[0];
            return response()->json($response);
        }
        $data = $request->all();
        $data_name = explode(" ", $data['name']);
        $data['name']=$data_name[0];
        
        $res['status']['code']    = 0;
        $res['status']['message'] = "user details with score.";
        
        $NMW = 50;
        $GMW = 0;
        $EMW = 0;
        $EVW = 0;
        $PCW = 0;
        $NMS = 0;
        $NMP = 0;
         try {
        $result = file_get_contents('https://truecheckr.com/api/phone-api?apiKey=5b2a21dba17f923T809w5sHSUEvBLeETNPPkescpHQT&phone_num='.$data['phone_num']);
       
         $result = json_decode($result, true);
         $result = $result['status'];
         
        
        $zerobounce       = file_get_contents('https://www.truecheckr.com/api/searchemail-api?email='.$data['email'].'&apiKey=5b2a226425a96Gal80GWvovFzk2RPAA9D78xjDUA45F');
        $zerobounce       = json_decode($zerobounce, true);
        $zerobounce_full= $zerobounce['status']['data'];
          $zerobounce =  $zerobounce['status']['data']['email_check'];
   
            $phone_name = explode(" ", $result['data']['value']['name']);
            similar_text(strtolower($data['name']), strtolower($phone_name[0]), $percent);
          
            if($percent>=50){ $NMP=25; }
            $NMS1 = $NMP*($percent/100);
           
            $NMP=0;
            similar_text(strtolower($data['name']), strtolower($zerobounce['firstname']), $percent);
            if($percent>=50){ $NMP=25; }
            $NMS2 = $NMP*($percent/100);
            $NMS = ($NMS1+$NMS2)/50;
        
            
            similar_text(strtolower($zerobounce['gender']), strtolower($result['data']['value']['gender_details'][0]['gender']), $percent);
            $GMS = $percent/100;
             
            $GMW = $GMS==1?5:0;
            
            $email1 = explode("@", $data['email']);
            $email2 = explode("@", $result['data']['value']['social_details'][0]['email']);
            similar_text(strtolower($email1[0]), strtolower($email2[0]), $percent);
            
            $EMS = $percent / 100;
            $EMW = $EMS==1?10:0;
            $EVS = 0;
            
            if ($zerobounce['status'] == 'Valid') {
                $EVS = 1;
                $EVW=25;
            }
            
            $PCS = 0;
            switch ($result['data']['value']['confidence']) {
                case 'low':
                    $PCS = 0.2;
                    $PCW = 0;
                    break;
                case 'avg':
                     $PCS = 0.5;
                     $PCW = 5;
                    break;
                case 'high':
                    $PCS = 1;
                    $PCW = 10;
                    break;
            }
            
            
            
           
            $name1 = strtolower($phone_name[0]);
            
            $name2   = strtolower($zerobounce['firstname']);
            $name    = strtolower($data['name']);
            $email   = $email1[0];
            $email1  = $email2[0];
            $gender  = strtolower($zerobounce['gender']);
            $gender1 = strtolower($result['data']['value']['gender_details'][0]['gender']);
            
            $msgg  = "";
            $codeg = "";
            if ($gender == $gender1) {
                $msgg  = "Gender Matched";
                $codeg = "11";
            } elseif ($gender == null || $gender1 == null) {
                $msgg  = "No information available";
                $codeg = "00";
            } elseif ($gender != $gender1) {
                $msgg  = "Gender Mismatched";
                $codeg = "10";
            } else {
                $msgg  = "No information available";
                $codeg = "00";
            }
            
            $msge  = "";
            $codee = "";
            
            if ($email == $email1) {
                $msge  = "Email Provided matched with phone provided email";
                $codee = "11";
            } elseif ($email1 == null) {
                $msge  = "No information available";
                $codee = "00";
            } else {
                $msge  = "Email Provided does not match with phone provided email";
                $codee = "10";
            }
            
            $msg  = "";
            $code = "";
            
            
             similar_text(strtolower($name), strtolower($name1), $percent);
             $name1= $percent/100;
             
             similar_text(strtolower($name), strtolower($name2), $percent);
             $name2= $percent/100;
            // die(json_encode ($name2));
            if ($name1>.75&&  $name2 >.75) {
                $msg  = "Name Provided Matches with email name and phone name";
                $code = "111";
            } elseif ($name1>.75 && $name2 != null) {
                $msg  = "Name Provided Matches with phone only";
                $code = "101";
                 
            } elseif ($name2>.75 && $name1 != null) {
                $msg  = "Name Provided Matches with email only";
                $code = "110";
                
            } elseif ($name1 == null && $name2 == null) {
                $msg  = "No Information available";
                $code = "122";
            } elseif ($name != $name2 && $name1 == null) {
                $msg  = "Name Provided Matches with email and Phone name unavailable ";
                $code = "112";
            } elseif ($name1>.75 && $name2 == null) {
                $msg  = "Name Provided Matches with phone and Email name unavailable";
                $code = "121";
            } else {
                $msg  = "Name Provided does not match with anything";
                $code = "110";
            }
            
            
            
            
            $devision = ($NMW + $GMW + $EMW + $EVW + $PCW);
            
            $total = ($NMW * $NMS) + ($GMW * $GMS) + ($EMW * $EMS) + ($EVW * $EVS) + ($PCW * $PCS);
            
            $res_score                               = $total / $devision;
            $res['data']['trustscore']['score']      = round($res_score,2);
            $res['data']['trustscore']['confidence'] =  "";
            $res['data']['name_match']['code']       = $code;
            $res['data']['name_match']['message']    = $msg;
            $res['data']['gender_match']['code']     = $codeg;
            $res['data']['gender_match']['message']  = $msgg;
            $res['data']['email_match']['code']      = $codee;
            $res['data']['email_match']['message']   = $msge;
            
             /*
            $res['data']['NMS'] = $NMS;
            $res['data']['GMS'] = $GMS;
            $res['data']['EMS'] = $EMS;
            $res['data']['EVS'] = $EVS;
            $res['data']['PCS'] = $PCS;
            
            $res['data']['NMW'] = $NMW;
            $res['data']['GMW'] = $GMW;
            $res['data']['EMW'] = $EMW;
            $res['data']['EVW'] = $EVW;
            $res['data']['PCW'] = $PCW;
            */
            
            
            if($res['data']['trustscore']['score'] > .7){
				  $res['data']['trustscore']['confidence']="High";
			}else if($res['data']['trustscore']['score']>.5) {
				  $res['data']['trustscore']['confidence']="Avg";
		 	}else {
				  $res['data']['trustscore']['confidence']="Low";
		 	}
            
            $res['data']['emailset'] = $zerobounce_full;
            $res['data']['phoneset'] = $result['data'];
            
            if (isset($GLOBALS['apikeyID'])) {
                $ipaddress = '';
                if (isset($_SERVER['HTTP_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
                else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
                else if (isset($_SERVER['HTTP_X_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
                else if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
                    $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
                else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
                else if (isset($_SERVER['HTTP_FORWARDED']))
                    $ipaddress = $_SERVER['HTTP_FORWARDED'];
                else if (isset($_SERVER['REMOTE_ADDR']))
                    $ipaddress = $_SERVER['REMOTE_ADDR'];
                else
                    $ipaddress = 'UNKNOWN';
                
                
                $apikeyID  = $GLOBALS['apikeyID'];
                $insert_db = array(
                    'apikeyID' => $apikeyID,
                    'requesttime' => time(),
                    'requestIP' => $ipaddress,
                    'response' => $res['data']
                    
                );
                
                
                
                DB::collection('apikeys')->where('_id', $apikeyID)->decrement('remcount');
                DB::collection('apis_analytics')->insertGetId($insert_db);
                
            }
            
            $res['data']['phoneset']['data']['value']['social_details'][1]['facebook_image'] = isset($res['data']['phoneset']['data']['value']['social_details'][1]['facebook_id']) ? 'https://graph.facebook.com/' . $res['data']['phoneset']['data']['value']['social_details'][1]['facebook_id'] . '/picture?type=large' : "";
            }
        catch (\Exception $e) {
            $res['status']['code']    = 2;
            $res['status']['message'] = "Information not available";
        } 
        
        die(json_encode($res));
    }
    
    
    
}
