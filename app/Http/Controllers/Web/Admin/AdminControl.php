<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\User;

class AdminControl extends Controller
{
    public function index(){
        return view('Admin.login');
    }

    public function get_settings(){
        return view('Admin.settings');
    }
        public function get_user_details(){

        return view('Admin.pages.user_create');
    }
    // public function authlogin_func(Request $request){
    //     $inputs    = $request->all();
    //     $rules     = array(
    //         'username' => 'required|max:255',
    //         'password' => 'required'
    //     );
    //     $validator = Validator::make($request->all(), $rules);
    //     $data      = $request->all();
    //     if ($validator->fails()) {
    //         return response()->json(array(
    //             'success' => false,
    //             'message' => $validator->getMessageBag()->toArray()
    //         ));
    //     } 
    //     $creds = [
    //         'username'          => $data['username'],
    //         'password'       => $data['password'],
    //         "roll_id"       => 100,
            
    //     ];

       
    //     if (Auth::attempt($creds, true)) {

                
                
    //             return response()->json(array(
    //                 'success' => true,
    //                 'message' => "Login successfully completed"
    //             ));
    //         }
            
    //     else
    //     {
    //     return response()->json(array(
    //         'success' => false,
    //         'message' => "Invalid user name or password"
    //     ));
    // }
    // }
    
    public function authlogin_func(Request $request){
        $inputs    = $request->all();
        $rules     = array(
            'username' => 'required|max:255',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        } 
        $userlog = DB::collection('users')->where('roll_id', 100)->orwhere('roll_id', 101)->where('username',$data['username'])->first();
       // die(json_encode(Hash::make($request->password)));   
        if ($userlog) {
            if (Hash::check($data['password'], $userlog['password'])) {
                Auth::attempt(array('userName' => $data['username'],'password' => $data['password'],'remember_token' =>(isset($data['rememberme']))? true : false));
                Auth::loginUsingId($userlog['_id']);
                return response()->json(array(
                    'success' => true,
                    'message' => "Login successfully completed"
                ));
            }
        }
        return response()->json(array(
            'success' => false,
            'message' => "Invalid user name or password"
        ));
    }
     public function change_settings(Request $request)
    {
        

        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        if (Hash::check($data['current_password'], Auth::user()->password)) {
            $user = User::find(Auth::id());


            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->username = $data['username'];
            $user->save();
           // Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }
    
    public function signout_func(){
		$userid=Auth::user()->roll_id;
		Auth::logout();
        if($userid==100)
        return redirect('/superadmin/');
        else
        return redirect('/my-account/');
    }


    public function user_func(Request $request)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        $roleId= 101;
         
           $user=User::where('username', '=', $request->username)->first();
            if ($user === null) {
  
                $user = new User;
                $n_password = Hash::make($request->new_password);
                $user->username=$request->username;
                $user->password = $n_password;
                $user->email=$request->email;
                 $user->mobile=$request->phone;
                $user->roll_id=$roleId;
                $user->delete=0;
                $user->save();
                return response()->json(array(
                'success' => true,
                'message' => 'Success', ));
                }

            else
                {
                return response()->json(array(
                'success' => false,
                'message' => 'username already Present', ));
              }


  }


  public function update_func(Request $request,$id)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required'
            
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
                    $user =User::where('_id', $id)->first();
           
              $user->username=$request->username;
              if($request->new_password!="********")
              {
                 $n_password = Hash::make($request->new_password);
                 $user->password = $n_password;
              }
                
                $user->mobile=$request->phone;
                $user->email=$request->email;
                $user->save();

                  return response()->json(array(
                'success' => true,
                'message' => 'successfully updated',));
   
         
           

  }

        public function user_details(Request $request){

        $roleId=101;
        $users = User::where('roll_id',$roleId)->where('delete',0)->paginate(10);
        foreach ($users as $user) {
        $user->registered_on = date('d-m-Y', strtotime($user->created_at));
        }
       return $users;
        
       }

          public function edituser_details(Request $request, $id){

              $user = User::where('_id', '=', $id)->first();
              $user->password_str="********";
              return $user;
           }



         public function delete_func(Request $request){

          $id=$request->id; 
          $user = User::where('_id',$id)->first();
          $user->delete=1;
          $user->save();
              return response()->json(array(
              'success'=>"true",
              'message' => " successfully delete"));
            
          }

}
