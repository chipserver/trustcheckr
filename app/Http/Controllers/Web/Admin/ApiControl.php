<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use Excel;
use Input;
use App\Repository\Lib\SearchControl;
use App\MasterPhone;
use App\Apikeys;
use App\Credits_add;
use Storage;
use Aws\Rekognition\RekognitionClient;
class ApiControl extends Controller
{
    public function api_keys(){
         return view('Admin.pages.api_keys');
    }
 public function pan_voter_dl(){
         return view('Admin.pages.api_keys_pan_dl_voter');
    }
    
     public function add_api_keys(){
     return view('Admin.pages.add_apikeys');
    }
    public function display_genderupload(){
         return view('Admin.pages.display_genderupload');
    }
    
    
    public function user_ratings(){
         return view('Admin.pages.user_ratings');
    }
     public function trustscrore_checker(){
             return view('Admin.pages.trustscrore_checker');
            }

    public function whatsapp_image(){
         return view('Admin.pages.whatsapp_image');
    }

    public function api_anlaytic(Request $request,$id){  
         $get=DB::collection('apis_analytics')->where('apikeyID',$id)->orderby('_id','DESC')->paginate(10);
         //die(json_encode( $get));
         return view('Admin.pages.api_analytics')->with("response",$get);
    }

     public function display_api_anlaytic($id){
        return $get=DB::collection('apis_analytics')->where('apikeyID',$id)->orderby('_id','DESC')->paginate(50);
        }



        public function  get_history_credits($id){
    $Credits_add =Credits_add::where('Apikeys_id',$id)->get();
 return view('Admin.pages.credits_history')->with(array('data'=>$Credits_add));

 }

public function post_apis_keys(Request $request){


  $inputs    = $request->all();

        $rules     = array(
            'name' => 'required',
            'category' => 'required',
             'count' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvwxyz";
           
           $randstr = '';
           for ($i=0; $i <30; $i++) {
               $randstr .= $alphabets[rand(0, strlen($alphabets) - 1)];
           }
           $key=uniqid().$randstr;

           $data=new Apikeys;
           $data->apiId=(int)$request->category;
           $data->name=$request->name;
           $data->key=$key;
           $data->setcount=(int)$request->count;
           $data->remcount=(int)$request->count;
           $data->user_id=$request->user;
           $data->save();
           return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
             
}



public function post_credits(Request $request){
    // die(json_encode("value"));
    $user=Apikeys::find($request->id);
   
   $user->setcount+=$request->count;
      $user->remcount+=$request->count;
    $user->save();
    $credits_used=new Credits_add;
    $credits_used->count=$request->count;
    $credits_used->Apikeys_id=$user->_id;
    $credits_used->save();
   return response()->json(array(
                    'success' => true,
                    'message' => " successfully completed",
                    'setcount'=> (int)$user->setcount,
                     'remcount'=>(int)$user->remcount,
                    'id'=>$user->_id
                ));
                    }

public function export_api_anlaytic($name,$id){
 
 ini_set('max_execution_time', 2400);
       
        $data= DB::collection('apis_analytics')->where('apikeyID',$id)->orderby('_id','DESC')->get();
         // die(json_encode( $data ));
         $filename='analytics_'.$name.time();
         Excel::create( $filename, function($excel) use($data) {
          $excel->sheet('Sheetname', function($sheet) use($data) {
          $sheet->row(1, array('slno',
          'phone', 'name', 'confidence','default_carrier','location','gender','city','email','facebook_id','facebook_name'));
           
            $i=2; 
            foreach($data as $info){ 

             $getdata=$info['response'];
             //die(json_encode($info['response']['value']));
             // if($info['res_state']==0){
                 
             // $res=PhoneSearch_lib::phonesearchlib_func($info);
      
             // if(count($res['status']['data'])>0){
                 
             //    DB::collection('bulk_child_phone')
             //    ->where('_id',$info['_id'])
             //    ->update(array(
             //             'res_state'=>1,
             //             'response'=>$res['status']['data'],
             //      )); 
                  
                 
             //      $getdata=$res['status']['data'];
             //     }
             //  } 

             
              
              $sheet->row($i++,array(
                    ($i-2),
                    $info['response']['key'],
                    isset($getdata['value']['name'])?$getdata['value']['name']:"",
                    isset($getdata['value']['confidence'])?$getdata['value']['confidence']:"",
                    isset($getdata['value']['number_details'][0]['default_carrier'])?$getdata['value']['number_details'][0]['default_carrier']:"",
                    isset($getdata['value']['number_details'][0]['location'])?$getdata['value']['number_details'][0]['location']:"",
                    isset($getdata['value']['gender_details'][0]['gender'])?$getdata['value']['gender_details'][0]['gender']:"",
                    isset($getdata['value']['location_details'][0]['city'])?$getdata['value']['location_details'][0]['city']:"",
                    isset($getdata['value']['social_details'][0]['email'])?$getdata['value']['social_details'][0]['email']:"",
                    isset($getdata['value']['social_details'][1]['facebook_id'])?$getdata['value']['social_details'][1]['facebook_id']:"",
                    isset($getdata['value']['social_details'][1]['facebook_name'])?$getdata['value']['social_details'][1]['facebook_name']:"",
                    
                    ));
                  
             
                 }
                
                 

          });
      })->export('xls');

        }


    public function get_api_anlaytic($id){
        return $get=DB::collection('apis_analytics')->where('_id',$id)->get();
        }

     public function import_export(){
     $get=DB::collection('excel_users_new')->orderby('_id','DESC')->get();
       return view('Admin.pages.import_export')->with("response",$get);
         
    }
    
    public function bulk_export1(Request $request){
        $get=DB::collection('excel_bulk_users')->orderby('_id','DESC')->get();
        return view('Admin.pages.bulk_import_export')->with("response",$get);
    }  
    
    public function importexcel_file_upload(Request $request){   
    $rules = array('name' => 'required','auth_truecaller' => 'required','import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
 
    set_time_limit(300);
   if($request->hasFile('import_file')){ 
      $post=$request->all();         
      $path = $request->file('import_file')->getRealPath();
      $data = Excel::load($path, function($reader) {})->get();
      $res['data']=array();
      $x=0;
	   if(!empty($data) && $data->count()){
       foreach($data as $value) { 
	  if($value['mobile']!=""){   
		   if($x==0){
		    $upid=DB::collection('excel_bulk_users')->insertGetId(array(
             'name'=> $post['name'],
             'auth_truecaller'=>$post['auth_truecaller'],
             'state'=>0,
             'total'=>count($data),
             'completed'=>0,
             'added_at'=>time(),
            ));}
                                     $insert['bulkId']=(string)$upid;
                                     $insert['mobile']=(string)$value['mobile'];
                                     $insert['res_state']=0;
                                     $insert['response']=array();
                                     DB::collection('excel_bulk_mobile')->insertGetId( $insert);
                                     $x++;
								                   }
                                    
                    }
        }
       
         return back()->with('success','Insert Record successfully.');
      }

    }
    

public function buk_export_download(Request $request, $u_id) {
	$data_req = $request->all();
    $master_data=DB::collection('excel_bulk_users')->where('_id',$u_id)->get();
  	$data=DB::collection('excel_bulk_mobile')->where('bulkId',$u_id)->get();
  
    $Bulk_TrueCallerAuth=$data_req['bulkauth']!=""?$data_req['bulkauth']:$master_data[0]['auth_truecaller'];
    Excel::create($u_id, function($excel) use($data,$u_id,$Bulk_TrueCallerAuth) {
    $excel->sheet('Sheetname', function($sheet) use($data,$u_id,$Bulk_TrueCallerAuth) {
      $sheet->row(1, array(
     'mobile', 'Name', 'Score','Address','Area','City','CountryCode','Email','Operator',
     'Timage'
     ));
     $i=2;
	  foreach($data as $info){

        $tcname=$tscore=$address=$area=$city=$countryCode=$email=$operator=$timage="";
        
       if(!isset($info['tcname'])) {  //die(json_encode($info ));
        $safeVision=array();
        $curl = curl_init('https://www.truecaller.com/api/search?type=4&countryCode=IN&q='.$info['mobile']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization:'.$Bulk_TrueCallerAuth));
        $result = curl_exec($curl);
        $result = json_decode($result,true);
       
 
        if(isset($result['data'][0])) {

         $tcname= isset($result['data'][0]['name']) ? $result['data'][0]['name']:"";
         $tscore=isset($result['data'][0]['score'])?$result['data'][0]['score']:0;
         
         $address=isset($result['data'][0]['addresses'][0]['address'])?$result['data'][0]['addresses'][0]['address']:"";
         $area=isset($result['data'][0]['addresses'][0]['area'])?$result['data'][0]['addresses'][0]['area']:"";
         $city=isset($result['data'][0]['addresses'][0]['city'])?$result['data'][0]['addresses'][0]['city']:"";
         $countryCode=isset($result['data'][0]['addresses'][0]['countryCode'])?$result['data'][0]['addresses'][0]['countryCode']:"";
         $email=isset($result['data'][0]['internetAddresses'][0]['id'])?$result['data'][0]['internetAddresses'][0]['id']:"";
         $operator=isset($result['data'][0]['phones'][0]['carrier'])?$result['data'][0]['phones'][0]['carrier']:"";
         $timage=isset($result['data'][0]['image'])?$result['data'][0]['image']:"";
         
           $master_mobile =  MasterPhone::firstOrNew(['mobile' => $info['mobile']]); 
           $master_mobile->mobile =$info['mobile'];
           $master_mobile->tcname  =$tcname;
           $master_mobile->tscore  =$tscore;
           $master_mobile->address =$address;
           $master_mobile->area = $area;
           $master_mobile->city = $city;
           $master_mobile->countryCode = $countryCode;
           $master_mobile->email = $email;
           $master_mobile->operator = $operator;
           $master_mobile->timage = $timage;
           $master_mobile->response =$result['data'][0];
           $master_mobile->save();
           
          DB::collection('excel_bulk_mobile')->where('bulkId',$u_id)
           ->update(['res_state'=>1]);
          
         // DB::collection('excel_users_new')->where('bulkId',$info['mobile'])->update( $insert);
     
        $udata['phone_num']=$info['mobile'];
        $res=array();
        $fakePhoto=false;
        $token= env('NeverExpireToken');
        $cookie=DB::collection('facebook_cookie')->first();   
        $cookie=$cookie['cookie'];  
        /*
        NOW TO JUST OPEN ANOTHER URL EDIT THE FIRST ARGUMENT OF THE FOLLOWING FUNCTION.
        TO SEND SOME DATA EDIT THE LAST ARGUMENT.
        */

        $html= SearchControl::loginfb("https://www.facebook.com/search/people/?q=%2B91".$info['mobile'],null,$cookie,null);
        $fbscrapes=array();
        $fbfound=false;
        $id=explode('data-bt="&#123;&quot;id&quot;:',$html);
        $foundid="";
        if(isset( $id[1])) { 
	        $fbfound=true;
          $id=explode(",&quot;",$id[1]);
          $fbscrapes['id']=$id[0];
          $foundid=   $fbscrapes['id'];
          $fb['searching_url'] = 'https://graph.facebook.com/'.$fbscrapes['id'].'?fields=name&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }

            $name = json_decode($res, TRUE);  
            $fbscrapes['name']= $name['name'];
            $profileurl='https://www.facebook.com/'.$id[0];
            $profileimage='https://graph.facebook.com/'.$id[0].'/picture?type=large';
            $fbscrapes['profileimage']=$profileimage;
        
	      }
  
        
        $search =array();
        if(!$fbfound){
            $search_name = str_replace(" ", "%20", $tcname); 
            $fb['searching_url'] = 'https://graph.facebook.com/v2.9/search?fields=name,id,picture.height(320)&limit=25&offset=0&type=user&q=' . $search_name . '&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }
           $search = json_decode($res, TRUE);  
            if (isset($search['data'])) {
		        if(count($search['data'])>0) {
               $res=$search['data'];
               $foundid=$search['data'][0]['id'];
  		        } 
            }
      
         }
         
         $insert=array('truecaller_res' => isset($result['data'][0])?$result['data'][0]:"", 
         );
        
         

           //die(json_encode( $res));
           DB::collection('excel_bulk_mobile')->where('_id',(string)$info['_id'])
           ->update([
            'tcname'=>$tcname,
            'tscore'=>$tscore,'address'=>$address,'city'=>$city,
            'area'=>$area,
            'city'=>$city,
            'countryCode'=>$countryCode,
               'email'=>$email,
            'operator'=>$operator,
             'timage'=>$timage,
            ]);
           
          }else {
			  
              $total_tc=count($data);
              $completed_tc=DB::collection('excel_bulk_mobile')->where('bulkId',$u_id)->where('tcname',"")->count(); 
              $display="<h3> ". $total_tc." out of   ". $completed_tc." is completed   </h3>";
               echo $display;
              die();
          


         }

         
         /*$tcname= isset($result['data'][0]['name']) ? $result['data'][0]['name']:"";
         $tscore=isset($result['data'][0]['score'])?$result['data'][0]['score']:0;
         $address=isset($result['data'][0]['addresses'][0]['address'])?$result['data'][0]['addresses'][0]['address']:"";
         $area=isset($result['data'][0]['addresses'][0]['area'])?$result['data'][0]['addresses'][0]['area']:"";
         $city=isset($result['data'][0]['addresses'][0]['city'])?$result['data'][0]['addresses'][0]['city']:"";
         $countryCode=isset($result['data'][0]['addresses'][0]['countryCode'])?$result['data'][0]['addresses'][0]['countryCode']:"";
         $email=isset($result['data'][0]['internetAddresses'][0]['id'])?$result['data'][0]['internetAddresses'][0]['id']:"";
         $operator=isset($result['data'][0]['phones'][0]['carrier'])?$result['data'][0]['phones'][0]['carrier']:"";
         $timage=isset($result['data'][0]['image'])?$result['data'][0]['image']:"";*/
         
        

        }else {
        
            $res= DB::collection('excel_bulk_mobile')->where('_id',(string)$info['_id'])->get();  
            // if($info['mobile']=="8973485788") die(json_encode($info['_id']));
            $tcname= $res[0]['tcname'];
            $tscore= $res[0]['tscore'];
            $address= $res[0]['address'];
            $area= $res[0]['area'];
            $city= $res[0]['city'];
            $email= $res[0]['email'];
            $operator= $res[0]['operator'];
            $timage= $res[0]['timage'];     
             
      
        }

      
           DB::collection('excel_bulk_users')->where('_id',$u_id)
            ->update(['completed'=>$i]);
       
           $sheet->row($i++, array(
             $info['mobile']." ",
             $tcname,
             $tscore,
             $address,
             $area, 
             $city,
             $countryCode,
             $email,
             $operator  ));

          // sleep(2);

        }
      
      

    });

})->export('xls');
  
  }
    public function import_upload(Request $request){   
    $rules = array('import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
 
    set_time_limit(300);
          
    if($request->hasFile('import_file')){         
      $path = $request->file('import_file')->getRealPath();

      $data = Excel::load($path, function($reader) {})->get();
                     
     $res['data']=array();
    
    $x=0;
	 
      if(!empty($data) && $data->count()){
        
         foreach($data as $value) { 
			 if($value['mobile']!=""){
                                    $insert['mobile']=$value['mobile'];
                                     $insert['res_state']=0;
                                     $insert['response']=array();
                                   
                                     array_push($res['data'],$insert);
                                  
                                    $x++;
								}
                                    
                                  }
        }
  
    if($x>100) {
		 return back()->with('error',"Please import 100 only records at time.. Limit exceeded try again");
	 }
      
        if(!empty($res['data'])){
         
          DB::collection('excel_users_new')->insertGetId(array(
             'users'=> $res['data'],
             'added_at'=>time(),
            ));
          return back()->with('success','Insert Record successfully.');
        }

      }

    }
    
   public function export_gender_check(Request $request){
    $rules = array('import_file' => 'required');
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
            $res=$validator->getMessageBag()->toArray();
            foreach ($res as $key => $value) {  return back()->with('error',$value[0]);  } 
    }
    set_time_limit(300);
    if($request->hasFile('import_file')){         
       $path = $request->file('import_file')->getRealPath();
       $data = Excel::load($path, function($reader) {})->get();
       $res['data']=array();
       
	   Excel::create(date('Y-m-d H:i:s'), function($excel) use($data) {
       $excel->sheet('Sheetname', function($sheet) use($data) {
       $sheet->row(1, array('url', 'fakePhoto', 'safeVision adult', 'safeVision spoof', 'safeVision medical', 
       'safeVision violence','AgeRange Low','AgeRange High','Gender','Gender Confidence'));
	 
      if(!empty($data) && $data->count()){
         $i=2;  
         foreach($data as $value) {
			 
			
			 if($value['image_url']!=""){ 
				        try { 
        $data['image_base64']=base64_encode(file_get_contents($value['image_url']));
        $res['requests'] = array();
        $res['requests']['image']['content'] = $data['image_base64'];
        $res['requests']['features'] = array();
        $res['requests']['features'][0]['type'] = "TEXT_DETECTION";
        $res['requests']['features'][1]['type'] = "SAFE_SEARCH_DETECTION";
        $res['requests']['features'][2]['type'] = "WEB_DETECTION";
        $data_string = json_encode($res);
        $visionAPiAuth= env('VisionAPiAuth');      
        $curl = curl_init('https://vision.googleapis.com/v1/images:annotate?key='.$visionAPiAuth);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json',));
        $result = curl_exec($curl);
        $result = json_decode($result, true);
        
         if (isset($result['error'])) {
            $info['status']['code'] = 2;
            $info['status']['message'] = "Invalid base64 image.";
            die(json_encode($info));
        }
        if (isset($result['responses'][0]['error'])) {
            $response['status']['code'] = 3;
            $response['status']['message'] = "Bad image data.";
            return response()->json($response);
        }
        $vresult = $result;
        
         
        $image = base64_decode($data['image_base64']);
        $img1 = time().'.jpg';
        $t = Storage::disk('s3')->put($img1,$image, 'public');
        $source_image = Storage::disk('s3')->url($img1);
      
        try {
        $client = RekognitionClient::factory(array(    
                         'credentials' => array(       
                         'key'    => env('S3_KEY'),
                         'secret' =>  env('S3_SECRET')),  
                         'region' =>  env('S3_REGION'),  
                         'version'=> 'latest',   
                      ));
          
        $result = $client->detectFaces([
            'Attributes' => ['ALL'],
            'Image' => [ 
            'Bytes' => file_get_contents($source_image),
         ],
       
        ]);  
        } catch (\Exception $e) {
                              $matching="";
           }
           $fakePhoto=false;
            if (isset($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'])) {
          
            if (strpos($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'][0]['url'], 'linkedin') !== true) {
                $fakePhoto=true;
            }  
          }
 
         $safeVision=$vresult['responses'][0]['safeSearchAnnotation'];
         $response['status']['code']=0;
         $response['status']['message']="Face detectFaces result";
         $response['data']['fakePhoto']=$fakePhoto;
         $response['data']['safeVision']=$safeVision;
         $response['data']['AgeRange']=isset($result['FaceDetails'][0]['AgeRange'])?$result['FaceDetails'][0]['AgeRange']:"";
         $response['data']['Gender']=isset($result['FaceDetails'][0]['Gender'])?$result['FaceDetails'][0]['Gender']:"";
        
         $sheet->row($i++, array(
             $value['image_url'],
             $fakePhoto,
             isset( $vresult['responses'][0]['safeSearchAnnotation']['adult'])?$vresult['responses'][0]['safeSearchAnnotation']['adult']:"",
             isset($vresult['responses'][0]['safeSearchAnnotation']['spoof'])?$vresult['responses'][0]['safeSearchAnnotation']['spoof']:"",
             isset($vresult['responses'][0]['safeSearchAnnotation']['medical'])?$vresult['responses'][0]['safeSearchAnnotation']['medical']:"",
             isset($vresult['responses'][0]['safeSearchAnnotation']['violence'])?$vresult['responses'][0]['safeSearchAnnotation']['violence']:"",
             isset($response['data']['AgeRange']['Low'])?$response['data']['AgeRange']['Low']:"",
             isset($response['data']['AgeRange']['High'])?$response['data']['AgeRange']['High']:"",
             isset($response['data']['Gender']['Value'])?$response['data']['Gender']['Value']:"",
             isset($response['data']['Gender']['Confidence'])?$response['data']['Gender']['Confidence']:""
          ));
          
          }catch (\Exception $e) {
			    $sheet->row($i++, array( $value['image_url'], ));
		  }
          
         
        //  break;
        
	      }
         }
        }
          });

})->export('xls');
        
      }  
	 }
    
    public function export(Request $request, $id)
  {
	
	$data=DB::collection('excel_users_new')->select('total','completed')->where('_id',$id) ->first();
	$data=DB::collection('excel_users_new')->where('_id',$id)->first();
    Excel::create(date('d-m-Y h:i:s',$data['added_at']), function($excel) use($data) {
    $excel->sheet('Sheetname', function($sheet) use($data) {
      $sheet->row(1, array(
     'mobile', 'TC Found', 'TC Name','TC Score','TC Email','TC Facebook','TC Carrier','TC Location','FB Found',
     'FB URL','FB ID','tagged_photos','liked_pictures','commented_pictures','tagged_posts','commented_posts','liked_posts',
     'liked_pages','visited_places'
     ));
     $i=2;
      foreach($data['users'] as $info){
       $safeVision=array();

       $curl = curl_init('https://www.truecaller.com/api/search?type=4&countryCode=IN&q='.$info['mobile']);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $Bulk_TrueCallerAuth= env('Bulk_TrueCallerAuth');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization:'.$Bulk_TrueCallerAuth));
        $result = curl_exec($curl);
        $result = json_decode($result,true);
        
 
        if(count($data)>1) { 	
         $tcname= isset($result['data'][0]['name']) ? $result['data'][0]['name']:"";
         $tscore=isset($result['data'][0]['score'])?$result['data'][0]['score']:0;
         $addresses=isset($result['data'][0]['addresses'])?$result['data'][0]['addresses']:"";
         $timage=isset($result['data'][0]['image'])?$result['data'][0]['image']:"";
         $email="";
         if(isset( $result['data'][0]['internetAddresses'])) {
           foreach ( $result['data'][0]['internetAddresses'] as $value) {
              if($value['service']=='email') $email=$value['id'];
            }
         }
       
         $udata['phone_num']=$info['mobile'];
               $res=array();
               $fakePhoto=false;
               $token= env('NeverExpireToken');
               
       
          if (isset($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'])) {
          
            if (strpos($vresult['responses'][0]['webDetection']['pagesWithMatchingImages'][0]['url'], 'linkedin') !== true) {
                $fakePhoto=true;
            }  
          }
          
          
        
           $cookie=DB::collection('facebook_cookie')->first();   
      
       $cookie=$cookie['cookie'];  
        /*
        NOW TO JUST OPEN ANOTHER URL EDIT THE FIRST ARGUMENT OF THE FOLLOWING FUNCTION.
        TO SEND SOME DATA EDIT THE LAST ARGUMENT.
        */

        $html= SearchControl::loginfb("https://www.facebook.com/search/people/?q=%2B91".$info['mobile'],null,$cookie,null);
        
        $fbscrapes=array();
        
        $fbfound=false;
        $id=explode('data-bt="&#123;&quot;id&quot;:',$html);
        
      
        $foundid="";
        if(isset( $id[1])) { 
	      $fbfound=true;
          $id=explode(",&quot;",$id[1]);
          $fbscrapes['id']=$id[0];
          $foundid=   $fbscrapes['id'];
          $fb['searching_url'] = 'https://graph.facebook.com/'.$fbscrapes['id'].'?fields=name&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }

            $name = json_decode($res, TRUE);  
            $fbscrapes['name']= $name['name'];
         
        $profileurl='https://www.facebook.com/'.$id[0];
        $profileimage='https://graph.facebook.com/'.$id[0].'/picture?type=large';
        $fbscrapes['profileimage']=$profileimage;
        
	    }
 
        
         $search =array();
        if(!$fbfound){
              
             $search_name = str_replace(" ", "%20", $tcname); 
           
           
            $fb['searching_url'] = 'https://graph.facebook.com/v2.9/search?fields=name,id,picture.height(320)&limit=25&offset=0&type=user&q=' . $search_name . '&access_token=' . $token;
            $res = @file_get_contents($fb['searching_url'], true);
            if ($res === false) {
                $res1['status']['code'] = 4;
                $res1['status']['message'] = "Token expired. ";
            }

            $search = json_decode($res, TRUE);  
            if (isset($search['data'])) {
		     if(count($search['data'])>0) {
             $res=$search['data'];
             $foundid=$search['data'][0]['id'];
		     } 
            }
      
        }
        
        
        
        $tagged_photos=$liked_pictures=$commented_pictures=$tagged_posts=$commented_posts=$liked_posts=$liked_pages=$visited_places=0;
        
        $scrapingdata=array();
        if($foundid!="") {
			
        /*
        NOW TO JUST OPEN ANOTHER URL EDIT THE FIRST ARGUMENT OF THE FOLLOWING FUNCTION.
        TO SEND SOME DATA EDIT THE LAST ARGUMENT.
        */
        
        
        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-of/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false   ) {
           $tagged_photos=1;
          
         } 
       
        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-of/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $liked_pictures=1;
        } 


        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/photos-commented/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $commented_pictures=1;
        } 



        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-tagged/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false  ) {
           $tagged_posts=1;
        } 

       

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-commented/intersect",null,$cookie,null);
        if (strpos($html, "BrowseResultsContainer") !== false  ) {
          $commented_posts=1;   
         
        } 


        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/stories-liked/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false ) {
           $liked_posts=1;
        } 

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/pages-liked/intersect",null,$cookie,null);
       if (strpos($html, "BrowseResultsContainer") !== false) {
           $liked_pages=1;
        } 

        $html= SearchControl::loginfb("https://www.facebook.com/search/".$foundid."/places-visited/",null,$cookie,null);
        if (strpos($html, "aboutInfo") !== false) {
           $visited_places=1;
        }   
          
       $scrapingdata['id']=$foundid;  
       $scrapingdata['tagged_photos']=$tagged_photos;
       $scrapingdata['liked_pictures']=$liked_pictures;
       $scrapingdata['commented_pictures']=$commented_pictures;
       $scrapingdata['tagged_posts']=$tagged_posts;
       $scrapingdata['commented_posts']=$commented_posts;
       $scrapingdata['liked_posts']=$liked_posts;
       $scrapingdata['liked_pages']=$liked_pages;
       $scrapingdata['visited_places']=$visited_places;
       }  
        $insert=array('id' => isset($result['data'][0]['id'])?$result['data'][0]['id']:"", 
                      'name' =>  $tcname, 
                      'score'=> $tscore,
                      'email'=> $email,
                      'image'=> $timage,
                      'addresses'=>isset($result['data'][0]['addresses'])?$result['data'][0]['addresses']:"", 
                      'image'=> isset($result['data'][0]['phones'])?$result['data'][0]['phones']:"",  
                      'safeVision'=>array($safeVision),
                      'fakePhoto'=> $fakePhoto,
                      'fbfound'=>$fbfound,
                      'fbdatainfo'=>array($fbscrapes),
                      'fbsearch'=>  $search, 
                       'datainfodata'=>array($scrapingdata)
                      );

       
        $response['status']['code'] = 0;
        $response['status']['message'] = "list of user data.";

        }else {
           $insert=array();
           $response['status']['code'] = 1;
           $response['status']['message'] = "No data found.";
        }
        $response['data']=$insert;
      
    
       
           $sheet->row($i++, array(
             $info['mobile']." ",
             isset($response['data']['name'])?$response['data']['name']:"",
             isset($response['data']['score'])? $response['data']['score']:"",
             isset($response['data']['email'])?$response['data']['email']:"",'','','','',
             isset($response['data']['fbfound'])?$response['data']['fbfound']:"",
             isset($response['data']['fbdatainfo'][0]['id'])?"https://facebook.com".$response['data']['fbdatainfo'][0]['id']:"",
             isset($response['data']['fbdatainfo'][0]['id'])?$response['data']['fbdatainfo'][0]['id']:"",
             isset($response['data']['datainfodata'][0]['tagged_photos'])?$response['data']['datainfodata'][0]['tagged_photos']:"",
              isset($response['data']['datainfodata'][0]['liked_pictures'])?$response['data']['datainfodata'][0]['liked_pictures']:"",
              isset($response['data']['datainfodata'][0]['commented_pictures'])?$response['data']['datainfodata'][0]['commented_pictures']:"",
              isset($response['data']['datainfodata'][0]['tagged_posts'])?$response['data']['datainfodata'][0]['tagged_posts']:"",
              isset($response['data']['datainfodata'][0]['commented_posts'])?$response['data']['datainfodata'][0]['commented_posts']:"",
              isset($response['data']['datainfodata'][0]['liked_posts'])?$response['data']['datainfodata'][0]['liked_posts']:"",
              isset($response['data']['datainfodata'][0]['liked_pages'])?$response['data']['datainfodata'][0]['liked_pages']:"",
              isset($response['data']['datainfodata'][0]['visited_places'])?$response['data']['datainfodata'][0]['visited_places']:"",
             
           ));
         
         sleep(10); 
         if($i==100)
         break;
         
       }
      
      

    });

})->export('xls');
  
  }
  
    public function bulk_export(Request $request)
  {
	    $id="5a41e8518c882d107d1124d2";
		    $data=DB::collection('excel_users_new')->where('_id',$id)->where('start',1)->first();
        Excel::create(date('d-m-Y h:i:s',$data['added_at']), function($excel) use($data,$id) {

			$excel->sheet('Sheetname', function($sheet) use($data,$id) {
	 		   $sheet->row(1, array(
			   'Mobile',
			 'Name', 'confidence', 'country code','default carrier','location','time_zone','gender','city','email','facebook_id',
			 'facebook_name','facebook_image' 
			 ));
            $i=2;
  
            
          foreach($data['users'] as $info){
			  
			  
        $curl = curl_init('https://app.peoplestacks.com/api/v1/lookup/phone/+91'.$info['mobile'].'?api_key=i7iHXFgQm3BM5Yg6L365kvSuBC012fUIJrm&api_token=o3W6K0Ehrv1WwXrHPoHl5d2czmceCoiQVW6');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $Bulk_TrueCallerAuth= env('Bulk_TrueCallerAuth');
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('authorization:'.$Bulk_TrueCallerAuth));
        $result = curl_exec($curl);
        $result = json_decode($result,true);
        
        
            $sheet->row($i++, array(
              $info['mobile'],
              isset($result['data']['value']['name'])?$result['data']['value']['name']:"",
              isset($result['data']['value']['confidence'])? $result['data']['value']['confidence']:"",
              isset($result['data']['value']['number_details'][0]['country_code'])?$result['data']['value']['number_details'][0]['country_code']:"",
              isset($result['data']['value']['number_details'][0]['default_carrier'])?$result['data']['value']['number_details'][0]['default_carrier']:"",
              isset($result['data']['value']['number_details'][0]['location'])?$result['data']['value']['number_details'][0]['location']:"",
              isset($result['data']['value']['number_details'][0]['time_zone'])?$result['data']['value']['number_details'][0]['time_zone']:"",
              isset($result['data']['value']['gender_details'][0]['gender'])?$result['data']['value']['gender_details'][0]['gender']:"",
              isset($result['data']['value']['location_details'][0]['city'])?$result['data']['value']['location_details'][0]['city']:"",
              isset($result['data']['value']['social_details'][0]['email'])?$result['data']['value']['social_details'][0]['email']:"",
            
              isset($result['data']['value']['social_details'][1]['facebook_id'])?$result['data']['value']['social_details'][1]['facebook_id']:"",
              isset($result['data']['value']['social_details'][1]['facebook_name'])?$result['data']['value']['social_details'][1]['facebook_name']:"",
              isset($result['data']['value']['social_details'][1]['facebook_image'])?$result['data']['value']['social_details'][1]['facebook_image']:"",
              
             
           ));
         
       
	 }
   });
   })->export('xls');
            
  }
     

}
