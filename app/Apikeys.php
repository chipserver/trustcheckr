<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Model;

class Apikeys extends Model  
{
     
    protected $table = 'apikeys';

      public function cat()
    {
        return  $this->hasMany('App\Apps_category', 'category_id','apiId');
    }
      public function user()
    {
        return  $this->belongto('App\User','user_id');
    }
}
