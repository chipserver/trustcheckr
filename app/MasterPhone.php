<?php

namespace App;
use Jenssegers\Mongodb\Eloquent\Model as Model;

class MasterPhone extends Model  
{
     
    protected $table = 'masterphone';

    protected $fillable = [
        'mobile'
        
    ];

}
