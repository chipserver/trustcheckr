<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class MasterKeys extends Model
{
    protected $table = "apikeys";
    protected $fillable = [
        'appiId',
        'name',
        'key'
     ];

    
}
