<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class MasterEmail extends Model
{
    
    protected $fillable = [
        'appiId',
        'name',
        'key'
     ];

    
}
