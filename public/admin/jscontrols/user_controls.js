var myApp = angular.module('tapmi', ['ngRoute']);

myApp.controller("UserController", ['$scope', '$http', function($scope, $http) {

  
    $(document).on('click', '.user_approve_control>ul>li', function() {
        var id = $(this).attr('id');
        $scope.updateStatus($(this).closest('tr').attr('id'),id);
    });
	
	 $(document).on('change', '[name=select_user_type]', function() {
		  $scope.userInfo($(this).val()) ;
     });
	
	 $(document).on('keyup', '[name=search-user]', function(e) {
		  var code = (e.keyCode ? e.keyCode : e.which);
                     if (code == 13|| $(this).val()) {
					   $scope.userInfo($('[name=select_user_type]').val());
                     }
		
     });
  
     $scope.userInfo = function(id) {  	 
		  $http.get('/superadmin/users/get/'+id).success(function(response) {  
			    $('.blead_tr').remove();   
			    $scope.details = response['data'];
			    $('tr.nodata').show();
			  
		  });
	 }
    $scope.updateStatus = function(userId,id) {  
		var statmnt=color=clsname="";
		 switch(parseInt(id)){
			case 100 : { statmnt ="approve"; clsname ="success";  color="rgb(132, 193, 153)"; break; }
			case 101 : { statmnt ="reject"; clsname ="danger"; color="rgb(181, 117, 117)";   break; }
		 }
		
		var statmnt=id==100?'appove':'reject';
        $.confirm({
            title: 'Confirm!',
            icon: 'fa fa-question-circle',
            content: 'Are you sure want to '+statmnt+' ?',
            type: 'orange',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Add',
                    btnClass: 'btn-orange',
                    action: function() {
                        var postdata = {
							userId: userId,
                            _token: $('meta[name="csrf_token"]').attr('content')
                        }
                       
                        $.post('/superadmin/users/update-state/'+id, postdata, function(response) {
							  if (response.status.code == 0) {
							  $('tbody.displayuser tr#'+userId).css('background-color',color).hide(1000);
							   }else{
						  $.alert({
                            title: 'Error!',
                            content: 'Something went wrong. <br> Try <strong>Again</strong> ',
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                           });
							  }
						
						}, 'json');
                    }
                },
                cancel: function() {}
            }
        });
    }

}]);
