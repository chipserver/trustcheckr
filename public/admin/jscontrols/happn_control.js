﻿var myApp = angular.module('profilecheck', ['ngRoute']);      
myApp.controller("HappnController", ['$scope', function($scope) { 
$(document).on('click', '[name=edit_profile_btn]', function(){
  var id=$(this).closest('tr').attr('id');
  $scope.editProfile(id); 
});
$(document).on('change keyup', '.form-control', function(){
          $(this).css('border-color', '');
            $('.message_area').html('');
});
$(document).on('click', '[name=update_profile_btn]', function(){
  var id=$(this).closest('tr').attr('id');
  $scope.updateProfile(id); 
});
$(document).on('click', '[name=delete_state_btn]', function(){
var id=$(this).closest('tr').attr('id');
$scope.deleteProfile(id); 
});
$(document).on('click', '[name=cancel_profile_btn]', function(){
 var id=$(this).closest('tr').attr('id');
 $scope.cancelProfile(id); 
});

$(document).on('click', '[name=like-msg]', function(){
  
       $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to Like all Profiles ?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Add',
                        btnClass: 'btn-orange',
                        action: function() {
                          $('.loaders button.loader-body').click();      
                        $.post('/superadmin/happn/like-profile', function(response) {
                          $('.loaders button.stop-loading').click(); 
                           $.alert({
    title: 'Success!',
    content: "Total "+response.data.total_like+" Liked  successfully. ",
});
                         },'json');
                     }
                    },
                  cancel: function() {}
                }
            });
});
$(document).on('click', '[name=send-msg]', function(){
        var option = "";
            for (i = 1; i <= 10; i++) {
                option += '<option value="' + i + '">' + i + '</option>';
            }
      $.confirm({
                            title: 'Submit Message.',
                            content: '<div><div class="form-group">'+
                                       '<label> Time Gap (sec) </label> &nbsp &nbsp ' +
                    '<select name="timegap" >' + option + '</select> <br/>' +
                                     '<input autofocus type="text" id="input-name" placeholder="Enter Your message" class="form-control">'+
                                     '<p class="text-danger help-block" style="display:none"></p>'+
                                     '</div>'+
                                     '</div>',
                            buttons: {
                                sayMyName: {
                                    text: 'Send Message',
                                    btnClass: 'btn-orange',
                                    action: function () {





          
 


                                        var input = this.$content.find('input#input-name');
                                        var errorText = this.$content.find('.text-danger');
                                        if (input.val() == '') {
                                            errorText.html('Please don\'t keep the message field empty!').slideDown(200);
                                            return false;
                                        } else {

                                           var timegap= $('[name=timegap] option:selected').val();
                                           $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to add ?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Add',
                        btnClass: 'btn-orange',
                        action: function() {
                            
   $('.loaders button.loader-body').click();  
                                             
                                                      var postdata={ message: input.val(),
                                                                     timegap:timegap, };
                                                    
                                                      $.post('/superadmin/happn/run-corn-job',postdata,function(response) {
                                                      $('.loaders button.stop-loading').click(); 
                                                      $.alert({
                                                            title: 'Success!',
                                                            content: "Total "+response.data.total_msg_sent+" message  successfully. ",
                                                        });
                                                       },'json');
                        }
                    },
cancel: function() {}
                }
            });


                                        }
                                    }
                                },
                                later: function () {
                                    // do nothing.
                                }
                            }
                        });
     
      
})



$(document).on('click', '.toggle-switch .ts-helper', function(){  
      var state=0;
      var id=$(this).attr('for'); 
      if ($('#'+id).prop('checked')) {
      var state=1;
      }
      var userid=$(this).closest('tr').attr('id');
       var postdata = {   'state': state,
                      'userid'  : userid   }
                     
      $('.loaders button.loader-body').click();   
      $.post('/superadmin/happn/set-user-state', postdata, function(response) {
      $('.loaders button.stop-loading').click(); 
      })
});
$scope.slno=function(){ 
  var i=1;
  $('[data-toggle="tooltip"]').tooltip(); 
  $('tbody tr:visible td:first-child').each(function() {
    $(this).html(i++)
});
}
    $scope.insertProfile = function() { 
        var proceed = true;
        $('[required=required]').filter(function() {
            if ($.trim(this.value) == "") {
                $(this).css('border-color', 'red');
                proceed = false;
            }
        });
        if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to add ?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Add',
                        btnClass: 'btn-orange',
                        action: function() {
                             var postdata = {
                                 token: $('[name=token ]').val(),
                                 lat: $('[name=lat ]').val(),
                                 log: $('[name=log ]').val(),
                             };
                 
                               $('.loaders button.loader-body').click();   
                            $.post('/superadmin/happn/insert', postdata, function(response) {
                                 $('.loaders button.stop-loading').click();  
                

                                if (response.status.code == 0) {
                                    var display = '<tr id=' + response.data.happn_user_id + '><td></td><td>' + response.data.name + '</td>' +
                                        '<td>' + response.data.token + '</td>'+
                                        '<td>' + response.data.lat + '</td>'+
                                        '<td>' + response.data.log + '</td>'+
                                        '<td><div class="toggle-switch"><input id="tw-switch' + response.data.happn_user_id + '" type="checkbox" hidden="hidden" checked>' +
                                        '<label for="tw-switch' + response.data.happn_user_id + '" class="ts-helper"></label>' +
                                        '</div></td>' +
                                        '<td class="">  <button type="button" name="view_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-eye zmdi-hc-fw"></span></button> <button type="button" name="edit_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-edit"></span></button> <button type="button" name="delete_state_btn" class="btn btn-icon command-delete" data-row-id="10253"><span class="zmdi zmdi-delete"></span></button></td></td>' ;  
                                    $('.display_state').append(display);
                  $scope.slno();
                                    $('.form-control').val('');
                                } else {

                                     $.alert({
    title: 'Error!',
    content:response.status.message,
});
                                }
                            }, 'json');

                        }
                    },
cancel: function() {}
                }
            });
        }

    }
  
   $scope.editProfile = function(id) {   $('.loaders button.loader-body').click();   
   $.get('/superadmin/happn/edit/'+id, function(response) {   $('.loaders button.stop-loading').click();   
     var display='<td></td><td></td>'+
                 '<td><textarea name="token" class="form-control input-sm" type="text" required="required" placeholder="Add Token">'+response[0].token+'</textarea>  </td>'+
                     '<td><input name="lat" class="form-control input-sm" type="text" required="required"  value="'+response[0].lat+'" /></td>'+
                     '<td><input name="log" class="form-control input-sm" type="text" required="required" value="'+response[0].log+'" /> </td>'+
                     

                 '<td colspan="2"><button type="button" name="update_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-check zmdi-hc-fw"></span></button> <button type="button" name="cancel_profile_btn" class="btn btn-icon command-delete" data-row-id="10253"><span class="zmdi zmdi-close zmdi-hc-fw"></span></button></td>';
         $('.display_state #'+id).html(display)
       $('tfoot').show();
                       $('[data-toggle="tooltip"]').tooltip(); 
       
    });
   }
     $scope.cancelProfile  = function(id) {   $('.loaders button.loader-body').click(); 
   $.get('/superadmin/happn/edit/'+id, function(response) {   
    var state="";
    if(response[0].state==0) {state="checked"; }
     $('.loaders button.stop-loading').click();   
     var display='<td></td><td></td>'+
                 '<td>'+response[0].token+'</td>'+
                     '<td>'+response[0].lat+'</td>'+
                     '<td>'+response[0].log+'</td>'+
                     '<td><div class="toggle-switch"><input id="tw-switch' + id + '" type="checkbox" hidden="hidden" '+state+'>' +
                                        '<label for="tw-switch' + id+ '" class="ts-helper"></label>' +
                                        '</div></td>' +
                      '<td class="">  <button type="button" name="view_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-eye zmdi-hc-fw"></span></button> <button type="button" name="edit_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-edit"></span></button> <button type="button" name="delete_state_btn" class="btn btn-icon command-delete" data-row-id="10253"><span class="zmdi zmdi-delete"></span></button></td></td>' ;  
                    $('.display_state #'+id).html(display)
             $('tfoot').show();
          $scope.slno();
      
    });
   }
   
     $scope.updateProfile = function(id) {
        var proceed = true;
        $('tbody [required=required]').filter(function() {
            if ($.trim(this.value) =="") {
                $(this).css('border-color', 'red');
                proceed = false;
            }
        });
        if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to update ?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Add',
                        btnClass: 'btn-orange',
                        action: function() {
                            var postdata = {
                                token: $('tbody [name=token]').val(),
                                lat:   $('tbody [name=lat]').val(),
                                log:   $('tbody [name=log]').val(),
                             };
                             $('.loaders button.loader-body').click();  
                            // alert(JSON.strigify()); 
               $.post('/superadmin/happn/update/'+id, postdata, function(response) {   
                                $('.loaders button.stop-loading').click(); 
                                if (response.status.code == 0) {
                                    var display = '<td></td><td>' + response.data.name + '</td>' +
                                        '<td>' + response.data.token + '</td>'+
                                        '<td>' + response.data.lat + '</td>'+
                                        '<td>' + response.data.log + '</td>'+
                                        '<td><div class="toggle-switch"><input id="tw-switch' + id+ '" type="checkbox" hidden="hidden" checked>' +
                                        '<label for="tw-switch' +id + '" class="ts-helper"></label>' +
                                        '</div></td>' +
                                        '<td class="">  <button type="button" name="view_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-eye zmdi-hc-fw"></span></button> <button type="button" name="edit_profile_btn" class="btn btn-icon command-edit" data-row-id="10253"><span class="zmdi zmdi-edit"></span></button> <button type="button" name="delete_state_btn" class="btn btn-icon command-delete" data-row-id="10253"><span class="zmdi zmdi-delete"></span></button></td>';
                                       $('.display_state tr#'+id).html(display);
                     $scope.slno();
                                  $('tfoot').show();
                                } else {
                                       $.alert({
                                        title: 'Error!',
                                        content:response.status.message,
                                    });
                                }
                            }, 'json');

                        }
                    },
                 cancel: function() {}
                }
            });
        }

    }

    $scope.deleteProfile = function(id) {
        $.confirm({
            title: 'Confirmation!',
            content: 'Are you sure want to Delete ?',
            icon: 'fa fa-question-circle',
            animation: 'scale',
            closeAnimation: 'scale',
            opacity: 0.5,
            buttons: {
                'confirm': {
                    text: 'Proceed',
                    btnClass: 'btn-danger',
                    action: function() {   $('.loaders button.loader-body').click();   
                        $.get('/superadmin/happn/delete/' + id, function(response) {    $('.loaders button.stop-loading').click();  
                            if (response.status.code == 0) {
                                $('.display_state tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                $scope.slno();
                                setTimeout(function() {
                                    if ($('.display_state tr:visible').length == 0) {
                                        var display = "<tr><th colspan=5 style='color:red'>No Records Found </th></tr>";
                                        $('table .display_state').html(display);
                                    }
                                }, 1001);

                            }
                        });
                    }
                },
              cancel: function() {}
            }
        });

    }

}]);