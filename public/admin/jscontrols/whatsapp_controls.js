
var myApp = angular.module('profilecheck', ['ngRoute']); 

myApp.controller("MatchController", ['$scope', '$http', function($scope, $http) {
   
     $scope.set_rating = function() {  
         var proceed = true;
            $('[name=search_matching] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });

      if (proceed) {
   
        var postdata = new FormData();
      
        postdata.append('image_type', $('[name=search_matching] [name=image_type]:checked').val());

        if(parseInt($('[name=image_type]:checked').val())==2) {  
           postdata.append('image_info', $('[name=search_matching] [name=image_info]').val());  
        }

        if(parseInt($('[name=image_type]:checked').val())==1) { 
           var fileList = $('[name=search_matching] [name=image_info]')[0].files[0];
           postdata.append('image_info', fileList);
        }  
       
       postdata.append('userId', $('[name=search_matching] [name=userId]').val()); 
       $('.loaders button.loader-body').click();   
       $http.post("/api/whatsapp/get-rating", postdata, {
                                transformRequest: angular.identity,
                                 headers: {
                                    'Content-Type': undefined
                                }
                                
        }).then(function(res, status, headers, config) {
   
        $('.loaders button.stop-loading').click();  
       
         if(res.data.status.code==0 || res.data.status.code==5 ) {
             $('[name=search_matching] #json_code').html(library.json.prettyPrint(res.data));
             $('[name=search_matching] .pre_json').show();
             
         }else {
             $('[name=search_matching] .pre_json').hide();
             $('[name=search_matching] #json_code').html('');
                     $.alert({
                            title: 'Error!',
                            content:res.data.status.message ,
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                        });
     }

}).error(function (data, status, headers, config) {
    $('.loaders button.stop-loading').click();  
});
     
      }
     }

   
	
	    $scope.update_rating = function() {  
           var proceed = true;
            $('[name=rating_cntl] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
    
      if (proceed) {
   
        var postdata = new FormData();
      
        postdata.append('imageId', $('[name=rating_cntl] [name=imageid]').val());
        postdata.append('rating',  $('[name=rating_cntl] [name=update_rating]').val());
        postdata.append('userId', $('[name=rating_cntl] [name=userId]').val());  
 
       $('.loaders button.loader-body').click();   
       $http.post("/api/whatsapp/update-rating", postdata, {
                                transformRequest: angular.identity,
                                 headers: {
                                    'Content-Type': undefined
                                }
                                
        }).then(function(res, status, headers, config) {
   
        $('.loaders button.stop-loading').click();  
       
         if(res.data.status.code==0) {
             $('[name=rating_cntl] #json_code').html(library.json.prettyPrint(res.data));
             $('[name=rating_cntl] .pre_json').show();
             
         }else {
             $('[name=rating_cntl]  .pre_json').hide();
             $('[name=rating_cntl]  #json_code').html('');
                     $.alert({
                            title: 'Error!',
                            content:res.data.status.message ,
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                        });
     }

}).error(function (data, status, headers, config) {
    $('.loaders button.stop-loading').click();  
});
     
      }
    }

	//update ratings
    $(document).on('click', '[name=update_ratings]', function() {       
          $scope.update_rating();
    })
	//image type
    $(document).on('click', '[name=get_score]', function() {       
          $scope.get_score();
    });

    //set rating
    $(document).on('click', '[name=set_rating]', function() {       
          $scope.set_rating();
    });
  
	
    //image type
    $(document).on('click', '[name=image_type]', function() {      
        var value=$(this).val();  
        var display="";
         switch(parseInt(value)) {
          case 1 : { display='<input type="file" class="form-control" name="image_info" required="true">'; break; } 
          case 2 : { display='<input type="text" class="form-control" name="image_info" placeholder="Enter text" required="true">'; break; } 

         }

         $('[name="image_input_cnt"]').html(display); 
    });
  
      
	

}]);