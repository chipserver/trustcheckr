﻿

var myApp = angular.module('profilecheck', ['ngRoute']); 

myApp.controller("MatchController", ['$scope', '$http', function($scope, $http) {

    $(document).on('click', '[name=submit]', function() {
           var proceed=true;
           ths=$(this);
    var id = $(this).attr("id");
      var count=$('#count').val();
          $('[name=credits_used] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                } 
              });
           if(proceed) {
                    var postdata = { "id": id,
                       "count": count,
                     };
                   
                      $.post('/superadmin/credits',postdata,function(response){         
                            if (response.success) {  
                                $('#myModal').modal('hide');
                    
                    $('tr#'+response.id).find('th span.tab').html(response.remcount+"/"+response.setcount);
                    // $('tr#'+response.id).find('td#remcount').html(response.remcount);
                         
                                
                             } 
                              else {
                                var display = "";
                        if ((typeof response.message) == 'object') {
                            $.each(response.message, function(key, value) {
                                display = value[0];
                            });
                        } else {
                            display = response.message;
                        }
                        $('[name=credits_used] [name=message_area]').html(display).css('color', 'red');
                
                    }
                },'json'); 
           }
    
      
   


 });
     
     $(document).on('click', '.more_cnt', function() {
  
            var id=$(this).closest('tr').attr('id');
            $('.loaders button.loader-body').click();   
            $.get('/superadmin/get-analytics/'+id, function(data) {  
           // alert(JSON.stringify(data[0].response)); 
                           $('.loaders button.stop-loading').click(); 
                           var jsons=  library.json.prettyPrint(data[0].response);  
                            var display='<pre ><code id="json_code">'+jsons +'</code></pre>'; 
                              
                             $.confirm({
                                title: 'search details',
                                content: display,
                                columnClass: 'medium',
                            }); // empty because its default.
                               
             },'json');
      })

     $scope.set_rating = function() {  
         var proceed = true;
            $('[name=search_matching] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
    
      if (proceed) {
   
        var postdata = new FormData();
      
        postdata.append('image_type', $('[name=search_matching] [name=image_type]:checked').val());

        if(parseInt($('[name=image_type]').val())==2) { 
           postdata.append('image_info', $('[name=search_matching] [name=image_info]').val());  
        }

        if(parseInt($('[name=image_type]').val())==1) { 
           var fileList = $('[name=search_matching] [name=image_info]')[0].files[0];
           postdata.append('image_info', fileList);
        }  
       
       postdata.append('userId', $('[name=search_matching] [name=userId]').val()); 
       $('.loaders button.loader-body').click();   
       $http.post("/api/get-rating", postdata, {
                                transformRequest: angular.identity,
                                 headers: {
                                    'Content-Type': undefined
                                }
                                
        }).then(function(res, status, headers, config) {
   
        $('.loaders button.stop-loading').click();  
       
         if(res.data.status.code==0 || res.data.status.code==5 ) {
             $('[name=search_matching] #json_code').html(library.json.prettyPrint(res.data));
             $('[name=search_matching] .pre_json').show();
             
         }else {
             $('[name=search_matching] .pre_json').hide();
             $('[name=search_matching] #json_code').html('');
                     $.alert({
                            title: 'Error!',
                            content:res.data.status.message ,
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                        });
     }

}).error(function (data, status, headers, config) {
    $('.loaders button.stop-loading').click();  
});
     
      }
     }

    $scope.get_score = function() {  
           var proceed = true;
            $('[name=search_matching] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
    
      if (proceed) {
   
        var postdata = new FormData();
      
        postdata.append('image_type', $('[name=search_matching] [name=image_type]:checked').val());

        if(parseInt($('[name=image_type]').val())==2) { 
           postdata.append('image_info', $('[name=search_matching] [name=image_info]').val());  
        }

        if(parseInt($('[name=image_type]').val())==1) { 
           var fileList = $('[name=search_matching] [name=image_info]')[0].files[0];
           postdata.append('image_info', fileList);
        }  

       $('.loaders button.loader-body').click();   
       $http.post("/api/search-scorenew", postdata, {
                                transformRequest: angular.identity,
                                 headers: {
                                    'Content-Type': undefined
                                }
                                
        }).then(function(res, status, headers, config) { 
   
      	$('.loaders button.stop-loading').click();  
       
         if(res.data.status.code==0) {
             $('#json_code').html(library.json.prettyPrint(res.data));
             $('.pre_json').show();
             
         }else {
             $('.pre_json').hide();
             $('#json_code').html('');
                     $.alert({
                            title: 'Error!',
                            content:res.data.status.message ,
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                        });
     }

}).error(function (data, status, headers, config) {
    $('.loaders button.stop-loading').click();  
});
     
      }
    }
	
	    $scope.update_rating = function() {  
           var proceed = true;
            $('[name=rating_cntl] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
    
      if (proceed) {
   
        var postdata = new FormData();
      
        postdata.append('imageId', $('[name=rating_cntl] [name=imageid]').val());
        postdata.append('rating',  $('[name=rating_cntl] [name=update_rating]').val());
        postdata.append('userId', $('[name=rating_cntl] [name=userId]').val());  
 
       $('.loaders button.loader-body').click();   
       $http.post("/api/update-rating", postdata, {
                                transformRequest: angular.identity,
                                 headers: {
                                    'Content-Type': undefined
                                }
                                
        }).then(function(res, status, headers, config) {
   
        $('.loaders button.stop-loading').click();  
       
         if(res.data.status.code==0) {
             $('[name=rating_cntl] #json_code').html(library.json.prettyPrint(res.data));
             $('[name=rating_cntl] .pre_json').show();
             
         }else {
             $('[name=rating_cntl]  .pre_json').hide();
             $('[name=rating_cntl]  #json_code').html('');
                     $.alert({
                            title: 'Error!',
                            content:res.data.status.message ,
                            icon: 'fa fa-rocket',
                            animation: 'zoom',
                            closeAnimation: 'zoom',
                            buttons: {
                                okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-blue'
                                }
                            }
                        });
     }

}).error(function (data, status, headers, config) {
    $('.loaders button.stop-loading').click();  
});
     
      }
    }

	//update ratings
    $(document).on('click', '[name=update_ratings]', function() {       
          $scope.update_rating();
    })
	//image type
    $(document).on('click', '[name=get_score]', function() {       
          $scope.get_score();
    });

    //set rating
    $(document).on('click', '[name=set_rating]', function() {       
          $scope.set_rating();
    });
  
	
    //image type
    $(document).on('click', '[name=image_type]', function() {      
        var value=$(this).val();  
        var display="";
         switch(parseInt(value)) {
          case 1 : { display='<input type="file" class="form-control" name="image_info" required="true">'; break; } 
          case 2 : { display='<input type="text" class="form-control" name="image_info" placeholder="Base64 image" required="true">'; break; } 

         }

         $('[name="image_input_cnt"]').html(display); 
    });
  
      

       $scope.updateApi = function() {
      
        var form = $('[name=add_api]');
        var proceed = $('[name=add_api]').parsley().validate();

        if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            var postdata = {};
                            var data = $('[name=add_api]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });
                               

                            $.post('/superadmin/post-apis-keys', postdata, function(response) {
                                if (response.success) {
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Details Updated',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                    // window.location='/superadmin/dashboard';
                                                     $('form[name=add_api]').get(0).reset();
                                                    $('form[name=add_api]').parsley().reset();
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* '+display).css('color', 'red');
                                }
                            }, 'json');
                        }
                    },
                    cancel: function() {}
                }
            });
        }
    }
	

}]);


myApp.controller("userController", ['$scope', '$http', function($scope, $http) {


    $scope.showSave = false;
    $scope.showBtns = true;


    //     $scope.reset = function() {
    // $('form[name=user_profile]').get(0).reset();

    //     }

    $scope.profile = function() {
        var form = $('[name=user_profile]');
        var proceed = $('[name=user_profile]').parsley().validate();

        if (proceed) {

            var postdata = {};
            var data = $('[name=user_profile]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });

 $("div#divLoading").addClass('show');
            $.post('/superadmin/user', postdata, function(response) {
                if (response.success) {
                     $("div#divLoading").removeClass('show');
                    $.alert({
                        title: 'Success!',
                        content: 'Vendor created successfully',
                        icon: 'fa fa-rocket',
                        animation: 'zoom',
                        closeAnimation: 'zoom',
                        buttons: {
                            okay: {
                                text: 'Okay',
                                btnClass: 'btn-blue',
                                action: function() {
                                    // ('user_profile').reset();
                                    $('form[name=user_profile]').get(0).reset();
                                    // window.location='/nightout/pgp-office/request';
                                    form.find('.resp-msg').html('  ');
                                    $scope.getUsers(1);
                                }
                            }
                        }
                    });
                } else {
                    $("div#divLoading").removeClass('show');
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    form.find('.resp-msg').html('* ' + display).css('color', 'red');
                }
            }, 'json');

        }
    }




    $scope.update = function(event) {


        var id = angular.element(event.currentTarget).attr('id');


        // alert(id);
        var form = $('[name=user_profile]');
        var proceed = $('[name=user_profile]').parsley().validate();

        if (proceed) {
            $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want to Update details?',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Yes',
                        btnClass: 'btn-orange',
                        action: function() {


                            var postdata = {};
                            var data = $('[name=user_profile]').serializeArray();
                            $.each(data, function() {
                                postdata[this.name] = this.value || '';
                            });

                         $("div#divLoading").addClass('show');

                            $.post('/superadmin/updateuser/' + id, postdata, function(response) {
                                if (response.success) {
                                     $("div#divLoading").removeClass('show');
                                    $.alert({
                                        title: 'Success!',
                                        content: 'Vendor updated successfully',
                                        icon: 'fa fa-rocket',
                                        animation: 'zoom',
                                        closeAnimation: 'zoom',
                                        buttons: {
                                            okay: {
                                                text: 'Okay',
                                                btnClass: 'btn-blue',
                                                action: function() {
                                                    // ('user_profile').reset();

                                                    $('form[name=user_profile]').get(0).reset();

                                                    // window.location='/nightout/pgp-office/request';
                                                    // form.find('.resp-msg').html('  ');
                                                    // $('[name="update"]').empty();
                                                    $scope.showSave = false;


                                                    $scope.getUsers(1);

                                                }
                                            }
                                        }
                                    });
                                } else {
                                    var display = "";
                                    if ((typeof response.message) == 'object') {
                                        $.each(response.message, function(key, value) {
                                            display = value[0];
                                        });
                                    } else {
                                        display = response.message;
                                    }
                                    form.find('.resp-msg').html('* ' + display).css('color', 'red');
                                }
                            }, 'json');
                        }
                    },
                    cancel: function() {}
                }
            });

        }
    }



    $scope.delete = function(event) {
       alert(event.target.id);
        var id = event.target.id;




        $.confirm({
            title: 'Confirm!',
            icon: 'fa fa-question-circle',
            content: 'Are you sure want to Delete details?',
            type: 'red',
            typeAnimated: true,
            buttons: {
                yes: {
                    text: 'Yes',
                    btnClass: 'btn-red',
                    action: function() {


                          $("div#divLoading").addClass('show');
                        $http.post('/superadmin/delete', {
                            'id': id
                        }).then(function(response) {
                          $("div#divLoading").removeClass('show');

                            $.alert({
                                title: " ",
                                content: "Vendor deleted Successfully",
                                // icon: 'fa fa-rocket',
                                animation: 'zoom',
                                closeAnimation: 'zoom',
                                buttons: {
                                    okay: {
                                        text: 'Okay',
                                        btnClass: 'btn-green',
                                        action: function() {
                                           $('#'+id).closest('tr').hide();

                                        }
                                    }
                                }
                            });

                        });




                    }
                },
                cancel: function() {}
            }
        });


    }


    $scope.edit = function(event) {


        var id = angular.element(event.currentTarget).attr('id');

        $http.get('/superadmin/edituser/' + id)
            .then(function(res) {
                var id = res.data._id;


                $('#email').val(res.data.email);
                $('#confirm_password').val(res.data.password_str);
                $('#new_password').val(res.data.password_str);



                 $('#phone').val(res.data.mobile);
                $('#username').val(res.data.username);


                // var change_pass = '<button type="button" id='+res.data._id +' name="update"  class="btn btn-primary waves-effect update">Update</button>';


                //         $('[name="update"]').empty().html(change_pass);

                $scope.showSave = true;

                $('.updatewarden').attr('id', id);


            });
    }



    // Pagination of Users
    $scope.maxSize = 3;
    $scope.$watch("currentPage", function() {

        $scope.getUsers($scope.currentPage);
    });
       $("div#divLoading").addClass('show');
    //load Users     
    $scope.getUsers = function(page = 1) {
        $http.get('/superadmin/user-details?page=' + page)
            .then(function(res) {
               $("div#divLoading").removeClass('show');
               // alert(JSON.stringify(res.data));
                $scope.users = res.data.data;

                $scope.totalUsers = res.data.total;
                $scope.currentPage = res.data.current_page;
                $scope.usersPerPage = res.data.per_page;
                $scope.usersFrom = res.data.from ? res.data.from : 0;
                $scope.usersTo = res.data.to ? res.data.to : 0;
            });
    }
      $scope.getUsers(1);
}]);


myApp.controller("TrustScoreController", ['$scope', '$http', function($scope, $http) {
 
 $(document).on('click', '.more_cnt', function() {
     var form = $('[name=trustscroe]');
        var proceed = $('[name=trustscroe]').parsley().validate();

        if (proceed) {
        var name=$('#name').val();
        var email=$('#email').val();
        var phone=$('#phone_num').val();
            // var id=$(this).closest('tr').attr('id');
            $('.loaders button.loader-body').click();   
            $.get('https://truecheckr.com/api/trustScore-new?phone_num='+phone+'&email='+email+'&name='+name+'&apiKey=kzpe5pCOKQjzwLHntKVsRyKmWvcyiP9TvnjBHIbBi4Y', function(data) {  
            // alert(JSON.stringify(data.data.emailset)); 
                           $('.loaders button.stop-loading').click(); 
                           var jsons=  library.json.prettyPrint(data.data);  
                            var display='<pre ><code id="json_code">'+jsons +'</code></pre>'; 
                              
                             $.confirm({
                                title: 'search details',
                                content: display,
                                columnClass: 'medium',
                            }); // empty because its default.

                              $('form[name=trustscroe]').get(0).reset();
                               
             },'json');
 
}

});

}]);