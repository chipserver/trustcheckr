var myApp = angular.module('messageapp', ['ngRoute']);      

myApp.controller("messageController", ['$scope', function($scope,$window) { 


$scope.PostMessage = function() {  
    var proceed = true;
            $('[required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });
        
           if (proceed) {
              $.confirm({
                title: 'Confirm!',
                icon: 'fa fa-question-circle',
                content: 'Are you sure want post message.',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    yes: {
                        text: 'Add',
                        btnClass: 'btn-orange',
                        action: function() {
                          var postdata= { 'image_info':$('#text_massage').val(),
                                          'image_type':2 }
                        $('.loaders button.loader-body').click();      
                        $.post('/api/whatsapp/get-rating', postdata,function(response) {
                        $('.loaders button.stop-loading').click(); 
                        location.href="/post/"+response.data.textId;
                       },'json');
                     }
                    },
                    cancel: function() {  }
                }
            });
}

}



$scope.RateMessage = function(ths,textId,like) {  
    var postdata= { 'textId':textId,
                    'rating':like  }
      //alert(JSON.stringify(postdata));
      //$('.loaders button.loader-body').click();      
      $.post('/api/whatsapp/update-rating',postdata,function(response) { 
     // $('.loaders button.stop-loading').click(); 
      if(response.status.code==0) {
                              
                                                   var count=parseInt(ths.closest('#'+textId).find('span.count_disp_'+like).html());
                                                 
                                                   count++;
                                                   ths.closest('#'+textId).find('span.count_disp_'+like).html(count);
                                                   ths.closest('#'+textId).find('[name=update_rating]#'+like).css('color','#007bff');
                                        
                                  }else {

                                    var jc= $.confirm({
                                        title: 'Error!',
                                        content: response.status.message,
                                        buttons: {
                                            Continue: {
                                                text: 'Close',
                                                btnClass: 'btn-blue',
                                                keys: ['enter', 'shift'],
                                                action: function() {
                                                   // location.reload();
                                               
                                            
                                                   jc.close();
                                                }
                                            }
                                        }
                                    });

                        }
     
    },'json');

}



$scope.CommentMessage = function(ths,comment,textId){
    var postdata= { 'textId':textId,
                  'comment':comment  }
      
      //$('.loaders button.loader-body').click();      
      $.post('/api/whatsapp/update-comments',postdata,function(response) { 
  //alert(JSON.stringify(postdata));
     // $('.loaders button.stop-loading').click(); 
      if(response.status.code==0) {
                              
                                                   var count=parseInt(ths.closest('#'+textId).find('span[name=comment_count]').html());
                                                 
                                                   count++;
                                                  // alert(count)
                                                   ths.closest('#'+textId).find('span[name=comment_count]').html(count);
                                                   ths.closest('#'+textId).find('[name=input_comments]').val('');
                                         var display='<div class="col-md-12" style="    padding-right: 35px; '+
                                          ' margin-left: 11px;    padding-top: 10px;">'+
                                           '<div class="message-inner">'+
                                            '<div class="message-head clearfix" style="border-bottom: none;">'+
                                              '<div class="avatar pull-left"><a href=""><img src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png" style="    border-radius: 50%;"></a></div>'+
                                              '<div class="user-detail">'+
                                                '<h5 class="handle">  '+response.data.userName+' </h5>'+
                                                '<div class="post-meta">'+
                                                  '<div class="asker-meta">'+
                                                    '<span class="qa-message-what"></span>'+
                                                     '<span class="qa-message-when">'+
                                                      '<span class="qa-message-when-data"> '+response.data.comment +'</span>'+
                                                    '</span>'+
                                                  '</div>'+
                                                '</div>'+
                                              '</div>'+
                                            '</div>'+
                                         '</div>'+
                                      '</div>';
                                      ths.closest('#'+textId).find('[name=comment_inputs] .listing').append(display);

                                        
                                  }else {

                                    var jc= $.confirm({
                                        title: 'Error!',
                                        content: response.status.message,
                                        buttons: {
                                            Continue: {
                                                text: 'Close',
                                                btnClass: 'btn-blue',
                                                keys: ['enter', 'shift'],
                                                action: function() {
                                                   // location.reload();
                                               
                                            
                                                   jc.close();
                                                }
                                            }
                                        }
                                    });

                        }
     
    },'json');

}



$(document).on('click', '[name=post_message]', function(){
  $scope.PostMessage(); 
});

$(document).on('click', '[name=update_rating]', function(){
  var like   =$(this).attr('id');
  var textId =$('#textId').val();
  $scope.RateMessage($(this),textId,like); 
});


$(document).on('click', '[name=time_update_rating]', function(){
  var like   =$(this).attr('id');
  var textId =$(this).closest('.textId_cnt').attr('id');
   $scope.RateMessage($(this),textId,like); 
});

$(document).on('click', '[name=post_comment]', function(){
   // alert('dsfd');
   var textId =$(this).closest('.textId_cnt').attr('id');
   var comment =$(this).closest('[name=comment_inputs]').find('[name=input_comments]').val();
 //  alert(comment);
   $scope.CommentMessage($(this),comment,textId); 
});



}]);