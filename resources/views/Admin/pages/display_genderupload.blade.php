 @extends('Admin.layouts.master_layout')
@section('content')
  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                     <li class="active"><a href="#scoreing" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"  >  Import Export Gender API</a></li>

                                   
                                 
                                </ul>
                              
                                <div class="tab-content" style="padding: 50px;">
                                 
                                  <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                                     <div  class="row">
										 
                                   <div class="col-sm-12">
									   
                                             <div class="col-md-2"><button type="button"    onclick="location.href='/sample_gender.xls';"   class="btn btn-primary" style=" top: 20px;left: -20px;">Sample Excel <i class="fa fa-download" aria-hidden="true"></i></button>
                                         </div>
                                      <div class="col-sm-8">
                                     <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                      
                      <form  action="/superadmin/export-gender-check" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="email">Upload Excel:</label>
    <input type="file"  name="import_file" class="form-control" id="email">
  </div>
  
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
                                   
 @if ($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
          </div>
        @endif
        @if ($message = Session::get('error'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
          </div>
        @endif


                                  </div>
                                  </div>
</div>
 
                                         
                                   
                                   
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>     
            <script src="/admin/jscontrols/api_controls.js"></script>
                         @endsection
