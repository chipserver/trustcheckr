 
 @extends('Admin.layouts.master_layout')
@section('content')

  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                     <li class="active"><a href="#scoreing" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"> Score Api </a></li>

                                     <li  ><a href="#analytics" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"> Mobile Api </a></li>

                                     <li><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="false"> Facebook Api </a></li>
                                    <li><a href="#email_key" aria-controls="email_key" role="tab" data-toggle="tab" aria-expanded="false"> Email Api </a></li>
                                    <li><a href="#stacks_phone" aria-controls="email_key" role="tab" data-toggle="tab" aria-expanded="false">  Stacks Phone</a></li>
                                    <li><a href="#zero_bounce" aria-controls="email_key" role="tab" data-toggle="tab" aria-expanded="false">  Zero bounce api </a></li>
                                   <li><a href="#trust_score" aria-controls="email_key" role="tab" data-toggle="tab" aria-expanded="false"> Trust score</a></li>
                               
                                
                                </ul>
                              
                                <div class="tab-content" style="padding: 50px;">
                                 
                                  <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>
                                            

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 
                                            @if( $data['apiId']==102 )
                                         <tr id="{{ $data['_id'] }}">
                                            <td>{{ $i++}}  </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th ><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span></th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>

                                   <div role="tabpanel" class="tab-pane " id="trust_score" name="trust_score">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==106)
                                         <tr id="{{ $data['_id'] }}" >
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span> </th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
                                 <div role="tabpanel" class="tab-pane " id="zero_bounce" name="zero_bounce">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==105)
                                         <tr id="{{ $data['_id'] }}" >
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span> </th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
                                  
                                   <div role="tabpanel" class="tab-pane " id="analytics" name="rating_cntl">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==100 )
                                         <tr id="{{ $data['_id'] }}" >
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th><span class="tab"> {{$data['remcount'] }} /  {{$data['setcount'] }} </span></th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>

 <div role="tabpanel" class="tab-pane " id="email_key" name="email_key">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==103 )
                                         <tr id="{{ $data['_id'] }}"> 
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                             <th> <span class="tab"> {{$data['remcount'] }} /  {{$data['setcount'] }} </span></th>
                                             <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
 <div role="tabpanel" class="tab-pane " id="stacks_phone" name="stacks_phone">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==104 )
                                         <tr id="{{ $data['_id'] }}">
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                             <th id="remcount"><span class="tab"> {{$data['remcount'] }} /  {{$data['setcount'] }}</span> </th>
                                              <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>

                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>


                                            <td><a href="/superadmin/export-analytics/{{ $data['name'] }}/{{ (string)$data['_id'] }}" class="btn btn-primary waves-effect" >Export</a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
                            <div role="tabpanel" class="tab-pane " id="home11" name="search_matching">
                            <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                          <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data)
                                            @if( $data['apiId']==101 )
                                         <tr id="{{ $data['_id'] }}">
                                            <td>{{ $i++ }}</td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{$data['name']}}</a></td>
                                             <th><span class="tab"> {{$data['remcount'] }} /{{$data['setcount'] }} </span> </th>
                                             <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }} </a></td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                    </div>




                                   
                                </div>
                            </div>
                            </div>
                 
                 
                </div>
            </section>     
            <script src="/admin/jscontrols/api_controls.js"></script>
           
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;">Update</h4>
        </div>
        <div class="modal-body">
        <form class="form-horizontal" name="credits_used" role="form">
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3">Count</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="count" placeholder="Enter Creadits " required="true" onkeypress='validate(event)'/>
                    </div>
                  </div>
              
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="button" name="submit" id="" class="btn btn-default">Submit</button>
                    </div>
                  </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</section>


<div class="loaders">
<button type="button" class="btn btn-default loader-body" data-target="body" style="display:none"></button>
<button type="button" class="btn btn-default" data-target="self" style="display:none"></button>
<button type="button" class="btn btn-default" data-target="form" style="display:none"></button>
<button type="button" class="btn btn-danger stop-loading" data-target="close" style="display:none"></button>
</div>

  

<script type="text/javascript">
function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
 $(document).on('click', '[name=myButton]', function() {
    $('#count').val('');
      
         var id = $(this).attr("id");
       
         $('[name=submit]').attr("id",id);
        
          $('#myModal').modal('show');
       });

</script>



@endsection
                         
