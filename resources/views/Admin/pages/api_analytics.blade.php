 @extends('Admin.layouts.master_layout')
@section('content')
<style type="text/css">
  
       pre {
       background-color: ghostwhite;
       border: 1px solid silver;
       padding: 10px 20px;
       margin: 20px; 
       overflow: hidden;
       }
    .json-key {
       color: brown;
       }
    .json-value {
       color: navy;
       }
    .json-string {
       color: olive;
       }

  
</style>


<script type="text/javascript">
 
 if (!library)
   var library = {};

library.json = {
   replacer: function(match, pIndent, pKey, pVal, pEnd) {
      var key = '<span class=json-key>';
      var val = '<span class=json-value>';
      var str = '<span class=json-string>';
      var r = pIndent || '';
      if (pKey)
         r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
      if (pVal)
         r = r + (pVal[0] == '"' ? str : val) + pVal + '</span>';
      return r + (pEnd || '');
      },
   prettyPrint: function(obj) {
      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
      return JSON.stringify(obj, null, 3)
         .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
         .replace(/</g, '&lt;').replace(/>/g, '&gt;')
         .replace(jsonLine, library.json.replacer);
      }
   };






     </script>

  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                            
                              
                                <div class="tab-content" style="padding: 50px;">

                                  
                                        @foreach (ApiKey::get() as $data) 

                                           @if((string)$data['_id']== Request::segment(3) )
                                           <p><b> Client Name </b>: {{ $data['name'] }}    </p>
                                           <p><b> Api Key </b>:    {{ $data['key'] }}    </p>
                                           <p><b> Hit Counts </b>: {{$data['remcount'] }} / {{$data['setcount'] }}  </p>
                                           @endif
                                        @endforeach

                                 <div role="tabpanel" class="tab-pane active" id="analytics" name="rating_cntl">
                                   <div class="card">
                                   <div  class="row">
                                   <div class="col-sm-12">
                                 
                                    <table class="table">
                                    <thead>
                                        <tr >
                                         
                                            <th>SLNo</th>
                                            <th> IP </th>
                                            <th> Date Time</th>
                                           </tr>
                                           </thead>
                                           <tbody class="append">

                                           <?php $i=1 ?>
                                            @foreach ($response  as $data) 
                                           <tr id="{{ $data['_id'] }}">
                                            <td>{{ $i++ }} </td>
                                            <td>{{ $data['requestIP'] }}</td>
                                            <td>{{ date('d-m-Y h:i:s A',$data['requesttime']) }}</td>
                                            <td><a href="javascript:void(0)" class="more_cnt"> more </a></td>
                                        </tr>

                                       
                                        @endforeach

                                         @if($i==1)  
                                         <tr >  <td colspan="4" style="color: red">No records found</td></tr> 
                                         @endif
                                         </tbody>
                                    </table>
                                    </div>
                                     <div id="pagination-demo" class="dataTables_paginate paging_bootstrap pagination" style="
    margin: 0px;
"><ul name="pagination"></ul></div>
                                  </div>
                              </div>
                            </div>
                          </div>
                         </div>
                    </div>
                  </div>
                     
                 
                </div>
            </section>    

            <script src="/admin/jscontrols/api_controls.js"></script>
            <script src="/admin/jscontrols/jquery.twbsPagination.min.js"></script>
         
<script type="text/javascript">
  
  
var page = "<?php $i = 1;  $page=ceil($response->total()/50); echo $page; ?>";
var id="<?php echo Request::segment(3)?>";

 

  function display(offset,id){
    $.get('/superadmin/display-analytics/'+id+'?page=' + offset, function(response) {
               
                var display = "";
                
                   // display +='<table class="table"> <thead> <tr><th>SLNo</th><th> IP </th> <th> Date Time</th></tr> </thead>';
               

                for (var i = 0; i < response.data.length; i++) {
                  
                 var dateTimeString = moment.unix(response.data[i].requesttime).format("DD-MM-YYYY HH:mm:ss");
                                   
                   display +='<tr id="'+response.data[i]._id['$oid']+'"><td>'+parseInt(parseInt(i + 1) + parseInt((offset - 1) * 50))+ '</td><td>'+response.data[i].requestIP+'</td><td>'+dateTimeString+'</td><td><a href="javascript:void(0)" class="more_cnt"> more </a></td></tr>';        
                                         
                            
                   
                    
                }

              display = display == "" ? '<tr><th colspan=4 style="color:red">No Records Found </th></tr>' : display;

                $('.append').html(display);

            }, 'json');

  }
  $('#pagination-demo').twbsPagination({

                totalPages: page,
                visiblePages: 6,
                onPageClick: function(event, page) {
                  
                 
display(page,id);
                  
                }
            });

</script>

             @endsection
