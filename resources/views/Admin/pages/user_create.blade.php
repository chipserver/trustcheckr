 @extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">


<!--  <section class="content-header">
      <h1>
        Vendor Creations
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/superadmin/dashbord"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Vendor Creations</li>
      </ol>
    </section> -->


    <section id="content"  ng-controller="userController" ng-clock>
      <div class="container">
                     <div class="card" style="padding: 10px;">
        <div class="row">
        <!-- left column -->
       


<div class="col-md-12">
<div class="col-md-5" style="border-right: 1px solid #edecec">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">User Creations</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" ng-enter="profile()" name='user_profile' ng-cloak>
              
                          <div class="box-body">
            {{csrf_field()}}


            <div class="form-group">
                 <label for="email" >Email address:</label>

                  
                     <input type="email"  required="required" name="email" id="email" class="form-control" placeholder="Email" data-parsley-errors-container="#email_format_error">
                 
                </div>


                  <div class="form-group">
                 <label for="email" >Mobile Number:</label>

                  
                     <input type="text"  required="required" name="phone" id="phone" class="form-control" placeholder="Mobile Number"     data-parsley-validation-threshold="1" data-parsley-trigger="keyup" 
    data-parsley-type="digits">
                 
                </div>

 
                <div class="form-group">
                  <label for="inputEmail3" >New Password</label>

                 
                    <input type="password"  required="required" name="new_password" id="new_password" class="form-control" placeholder="New Password" data-parsley-errors-container="#new_pass_error">
    
                           
                            
                            <div id="new_pass_error"></div>
          
                </div> 
                         
                         
                            <div class="form-group">
                  <label for="inputEmail3" >Confirm Password</label>

                 
                    <input type="password"  value="" required="required" data-parsley-equalto="#new_password" data-parsley-equalto-message="Confirm Password should match the New Password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Repeat Password" data-parsley-errors-container="#rep_pass_error">

                            <div id="rep_pass_error"></div>
                  
                </div>      
                          
                      
                    


              <div class="form-group">
                                
                                  <label for="user" >User Name</label>
                                  
                                    <input type="username"  id="username" required="required" name="username" class="form-control" placeholder="Username">
                              
                            </div>
   
  
  
  <div class="resp-msg"></div>

  <div class="box-footer">
                <button  type="reset"   class="btn btn-default waves-effect" >Reset</button>

             <button type="button" ng-if="showBtns" ng-hide="showSave " ng-click="profile()"  class="btn btn-primary waves-effect pull-right">Submit</button>
                               
                                <button type="button" ng-if="showBtns" ng-show="showSave"   ng-click="update($event)" id=""   name="update" class="btn btn-primary updatewarden waves-effect pull-right">Update</button>

              </div>
  
                                </div>
</form>

                     </div>
                     </div>


 <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border" >
              <h3 class="box-title">Total Users ( @{{totalUsers}} )</h3>
            </div>
           
            <div class="box-body">
              <table class="table ">
                <tbody>  <tr >
                                        <th width="5%" style="padding-left: 0px">Sl_no</th>
                                        <th>Email_ID</th>
                                        <th>User_Name</th>
                                        <th>Mobile</th>
                                                                         
                                        <th>Create_Date</th>
                                        <th colspan="2">Actions</th>
                                    </tr>


                                      <tr ng-repeat="user in users" id="@{{user._id}}">
                                  <td  style="padding-left: 0px">@{{(usersPerPage * (currentPage-1)) + $index+1 }}</td>
                                    <td>@{{user.email}}</td>
                                    
                                     <td>@{{user.username}} </td>
                                       <td>@{{user.mobile}} </td>
                                    <td>@{{user.registered_on}} </td>
                                    <td>

            <button id="@{{user._id}}" class="btn-primary" ng-click="edit($event)">
                          <i class="zmdi zmdi-edit"></i>
                        </button>
                                    </td>
                                    <td>
                            <button id="@{{user._id}}" class="btn-danger" data-ng-click="delete($event)">
                         <i class="zmdi zmdi-delete"></i>
                        </button>
                                            


                                        </td>
                                </tr>
                                   
               
              </tbody></table>
            </div>
      

            <div class="box-footer clearfix">
             <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>
     
            </div>
          </div>


          </div>
          </div>
                     </div>    
          </section>



       

  
                                    



    <script src="/admin/jscontrols/api_controls.js"></script>

<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>
<script type="text/javascript">
   function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
</script>







    @endsection