 @extends('Admin.layouts.master_layout')
@section('content')
  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                     <li class="active"><a href="#scoreing" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"  >  Import Export </a></li>

                                   
                                 
                                </ul>
                              
                                <div class="tab-content" >
                                 
                                  <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                                     <div  class="row">
										 
                                   <div class="col-sm-12">
									   
                                           
                                      <div class="col-sm-12">
                                     <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                      
                      <form  action="/superadmin/importexcel-file" method="post" enctype="multipart/form-data">
                       <div class="col-md-3">
   <div class="form-group">
    <label for="email"> Name</label>
    <input type="text"  name="name" class="form-control">
  </div>
  </div>
  <div class="col-md-3">
   <div class="form-group">
    <label for="email">Auth Truecaller</label>
    <input type="text"  name="auth_truecaller" class="form-control">
  </div>
  </div>
   <div class="col-md-2">
   <div class="form-group">
    <label for="email">Upload Excel:</label>
    <input type="file"  name="import_file" class="form-control"  >
  </div>
  </div>
 
  <div class="col-md-2">

  <button type="submit" class="btn btn-default">Submit</button>
   </div>
     <div class="col-md-2"><button type="button"    onclick="location.href='/sample.xls';"   class="btn btn-primary" style=" top: 20px;left: -20px;">Sample Excel <i class="fa fa-download" aria-hidden="true"></i></button>
                                         </div>
</form>
         <div class="col-md-8">                            
 @if ($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
          </div>
        @endif
        @if ($message = Session::get('error'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
          </div>
        @endif
  </div >

                                  </div>
                                  </div>
</div>
 
                                         
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                            <th> Auth TC</th>
                                            <th> Date Time</th>
                                            <th> States </th>
                                            <th> Download </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach ($response as $data) 
                                            
                                         <tr>
                                             <td>{{$i++ }}</td>
                                             <td>{{ $data['name'] }}</td>
                                             <td>{{ $data['auth_truecaller'] }}</td>
                                              <td>{{ date('d-m-Y h:i:s A',$data['added_at']) }} </td>
                                             <td> 
                                                  @if($data['state']==0)
                                                  <span style="color:red">Pending  </span>
                                                  @elseif($data['state']==1)
                                                   <span style="color:blue">Processing  </span>
                                                 @else 
                                                    <span style="color:green">Pending  </span>
                                                  @endif

                                              </td>
                                           
                                            <td>
                                             
                                             <a href="/superadmin/bulkexport-download/{{(string)$data['_id']}}?bulkauth=" target="blank"> Download </a>
                                            

                                            </td>
                                        </tr>
                                        
                                          
                                          @endforeach
                                            </table>
                                          </div>
                                        </div>
                                    </div>
                                  </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>     
            <script src="/admin/jscontrols/api_controls.js"></script>
                         @endsection
