 
 @extends('Admin.layouts.master_layout')
@section('content')

  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                 <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                     <li class="active"><a href="#pan" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"> PAN Api </a></li>
                                     <li><a href="#voter" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false"> VOTER Api </a></li>
                                     <li><a href="#dl" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="false"> DL Api </a></li>
                                  </ul>
                              
                                <div class="tab-content" style="padding: 50px;">
                                 
                                  <div role="tabpanel" class="tab-pane active" id="pan"  >
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>
                                            

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 
                                            @if( $data['apiId']==107)
                                         <tr id="{{ $data['_id'] }}">
                                            <td>{{ $i++}}  </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th ><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span></th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>

                                   <div role="tabpanel" class="tab-pane " id="voter" name="voter">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==109)
                                         <tr id="{{ $data['_id'] }}" >
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span> </th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
                                 <div role="tabpanel" class="tab-pane " id="dl" name="dl">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                  
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Name</th>
                                             <th colspan="2"> Hit Counts </th>
                                            <th> Key </th>

                                        </tr>
                                          </thead>
                                           <?php $i=1 ?>
                                            @foreach (ApiKey::get() as $data) 

                                           @if( $data['apiId']==108)
                                         <tr id="{{ $data['_id'] }}" >
                                            <td>{{ $i++}} </td>
                                            <td><a href="/superadmin/history-credits/{{ $data['_id'] }}" target="blank">{{ $data['name'] }}</a></td>
                                              <th><span class="tab"> {{$data['remcount'] }} / {{$data['setcount'] }} </span> </th>
                                               <td>  <button type="button" class="btn btn-info btn-sm" name="myButton" id="{{ $data['_id'] }}"><i class="zmdi zmdi-plus"></i></button> </td>
                                            <td><a href="/superadmin/analytics/{{ (string)$data['_id'] }}" target="blank">{{ $data['key'] }}  </a></td>

                                        </tr>
                                        @endif
                                        @endforeach
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>
                                  
                                   

  
   




                                   
                                </div>
                            </div>
                            </div>
                 
                 
                </div>
            </section>     
            <script src="/admin/jscontrols/api_controls.js"></script>
           
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;">Update</h4>
        </div>
        <div class="modal-body">
        <form class="form-horizontal" name="credits_used" role="form">
                  <div class="form-group">
                    <label  class="col-sm-2 control-label"
                              for="inputEmail3">Count</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" 
                        id="count" placeholder="Enter Creadits " required="true" onkeypress='validate(event)'/>
                    </div>
                  </div>
              
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="button" name="submit" id="" class="btn btn-default">Submit</button>
                    </div>
                  </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
</section>


<div class="loaders">
<button type="button" class="btn btn-default loader-body" data-target="body" style="display:none"></button>
<button type="button" class="btn btn-default" data-target="self" style="display:none"></button>
<button type="button" class="btn btn-default" data-target="form" style="display:none"></button>
<button type="button" class="btn btn-danger stop-loading" data-target="close" style="display:none"></button>
</div>

  

<script type="text/javascript">
function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
 $(document).on('click', '[name=myButton]', function() {
    $('#count').val('');
      
         var id = $(this).attr("id");
       
         $('[name=submit]').attr("id",id);
        
          $('#myModal').modal('show');
       });

</script>



@endsection
                         
