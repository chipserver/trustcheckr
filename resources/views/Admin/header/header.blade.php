<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> TrustCheckr Admin</title>
        <!-- Vendor CSS -->
        <link href="/assests/css/animate.min.css" rel="stylesheet">
        <link href="/assests/css/sweet-alert.css" rel="stylesheet">
        <link href="/assests/css/material-design-iconic-font.min.css" rel="stylesheet">
        <!-- CSS -->
        <link href="/assests/css/app.min.admin1.css" rel="stylesheet">
        <link href="/assests/css/app.min.2.css" rel="stylesheet">
        <link rel="stylesheet" href="/assests/css/jquery-confirm.min.css">
        <script src="/assests/js/jquery.min.js"></script>
        <script src="/assests/lib/angular/angular.min.js"></script>
        <script src="/assests/lib/angular/route.js"></script>
        <script src="/assests/js/jquery-confirm.min.js"></script>
   
    </head>
