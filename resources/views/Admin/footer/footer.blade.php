
  <div class="loaders">
            <button type="button" class="btn btn-default loader-body" data-target="body" style="display:none"></button>
            <button type="button" class="btn btn-default" data-target="self" style="display:none"></button>
            <button type="button" class="btn btn-default" data-target="form" style="display:none"></button>
            <button type="button" class="btn btn-danger stop-loading" data-target="close" style="display:none"></button>
        </div> 

  
        <script src="/assests/js/custom-setting.js"></script>
        <script src="/assests/js/jquery-loader.js"></script>
        <script src="/assests/js/bootstrap.min.js"></script>
        <script src="/assests/js/jquery.flot.js"></script>
        <script src="/assests/js/jquery.flot.resize.js"></script>
        <script src="/assests/js/curvedLines.js"></script>
        <script src="/assests/js/jquery.sparkline.min.js"></script>
        <script src="/assests/js/jquery.easypiechart.min.js"></script>
        <script src="/assests/js/moment.min.js"></script>
        <script src="/assests/js/fullcalendar.min.js"></script>
        <script src="/assests/js/jquery.simpleWeather.min.js"></script>
        <script src="/assests/js/jquery.nicescroll.min.js"></script>
        <script src="/assests/js/waves.min.js"></script>
        <script src="/assests/js/bootstrap-growl.min.js"></script>
        <script src="/assests/js/sweet-alert.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="/assests/js/curved-line-chart.js"></script>
        <script src="/assests/js/line-chart.js"></script>
        <script src="/assests/js/charts.js"></script>
        
        <script src="/assests/js/charts.js"></script>
        <script src="/assests/js/functions.js"></script>
        <script src="/assests/js/demo.js"></script>