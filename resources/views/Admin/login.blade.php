<!DOCTYPE html>
    <!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>TrustCheckr Admin</title>
         <!-- Vendor CSS -->
        <link href="/assests/css/animate.min.css" rel="stylesheet">
       
        <link href="/assests/css/material-design-iconic-font.min.css" rel="stylesheet">
        <!-- CSS -->
        <link href="/assests/css/app.min.1.css" rel="stylesheet">
        <link href="/assests/css/app.min.2.css" rel="stylesheet">
      
    <script src="assests/js/jquery.min.js"></script>
         <script src="assests/lib/angular/angular.min.js"></script>
        <script src="assests/lib/angular/route.js"></script>
    </head>
    
    <body ng-app="ProfileCheckAdmin" class="login-content">
        <!-- Login -->
        <div class="lc-block toggled" name="login-inputs" id="l-login" ng-controller="LoginController">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line"> <h3>TrustCheckr Admin</h3>
                    <input type="text" class="form-control" placeholder="Username" name="username" required="true">
                </div>
            </div>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                <div class="fg-line">
                    <input type="password" class="form-control" placeholder="Password" name="password" required="true">
                </div>
            </div>
            
            <div class="clearfix"></div>
            
           
             <div name="message_area">
                <label>
                  
                </label>
            </div>
            
            <a href="javascript:void(0)" class="btn btn-login btn-danger btn-float" name="submit-login"><i class="zmdi zmdi-arrow-forward"></i></a>
            
           
        </div>
        
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->

        <script src="/assests/js/jquery.min.js"></script>
        <script src="/assests/js/bootstrap.min.js"></script>

        <script src="/admin/jscontrols/app_controls.js"></script>
        <script src="/admin/jscontrols/login_controls.js"></script>

        <script src="/assests/js/waves.min.js"></script>
        <script src="/assests/js/functions.js"></script>
         
    </body>
</html>
