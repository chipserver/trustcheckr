<!DOCTYPE html>
    @include('Admin.header.header')
             
    <body class="toggled sw-toggled" ng-app="profilecheck">
        <header id="header">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>
               <li class="logo hidden-xs">
                    <a href="Javascript:void(0)">API  Trackers</a>
                </li>
                <li class="pull-right">
            <!-- Top Search Content -->
            <div id="top-search-wrap">
                <input type="text">
                <i id="top-search-close">&times;</i>
            </div>
        </header>
        <style type="text/css">
    pre {
   background-color: ghostwhite;
   border: 1px solid silver;
   padding: 10px 20px;
   margin: 20px; 
   }
.json-key {
   color: brown;
   }
.json-value {
   color: navy;
   }
.json-string {
   color: olive;
   }

  </style>
 
        <section id="main">
            <aside id="sidebar">
                <div class="sidebar-inner c-overflow">
                    <div class="profile-menu">
                        <a href="">
                            <div class="profile-pic">
                                <img src="http://www.profilecheckr.com/Website/images/default.png" alt="">
                            </div>
                             <div class="profile-info">
                               Admin
                               <i class="zmdi zmdi-arrow-drop-down"></i>
                            </div>
                        </a>
                        <ul class="main-menu">
                            <li><a href="/superadmin/settings"><i class="zmdi zmdi-account"></i> Settings </a> </li>
                            <li>  <a href="/superadmin/logout"><i class="zmdi zmdi-time-restore"></i> Logout</a> </li>
                        </ul>
                    </div>

                   <ul class="main-menu">
						    @if(Auth::user()->roll_id==100)
                             <li class=" ">   <a href="/superadmin/dashboard"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Home </a></li>
                            <li class=" ">   <a href="/superadmin/apis-keys"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  App's  </a></li> 
                              <li class=" ">   <a href="/superadmin/trustscrore-checker"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Trustscrore   </a></li> 
 <li class=" ">   <a href="/superadmin/pan-voter-dl-keys"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i> PAN/VOTER/DL </a></li> 
                            <li class=" "><a href="/superadmin/add-apis-keys"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Add Apis Keys </a></li> 

                            <li class=" "><a href="/superadmin/import-export"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Import/Export API </a></li> 
 <li class=" "><a href="/superadmin/bulkexport"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Bulk Upload </a></li> 
 <li class=" "><a href="/superadmin/gender-upload"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Gender Upload </a></li> 

  <li class=" "><a href="/superadmin/bulk-email-upload"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Bulk email upload </a></li> 
  <li class=" "><a href="/superadmin/bulk-phone-upload"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  Bulk  phone upload </a></li> 
   <li class=" "><a href="/superadmin/userdetails"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i>  User Details </a></li> 
                          @else 
                            @foreach(App\Apikeys::where('user_id',Auth::user()->_id)->with('cat')->get() as $data)
                            
                             @foreach($data->cat as $da)
                            <li class=""><a href="/my-account/analytics/{{$data->_id}}">
                          
                             <i class="zmdi zmdi-star-half zmdi-hc-fw"></i>
                            {{$da->category_name}}  </a></li>
                             @endforeach 
                          @endforeach
                      <!--     <li class=""><a href="/my-account/analytics/5a954fbd3ba3af7f303441fd"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i> Phone hits tracking  </a></li>  -->
                         <li></li>
                          @endif
                    </ul>
                </div>
            </aside>
            
            <aside id="chat">
                <ul class="tab-nav tn-justified" role="tablist">
                    <li role="presentation" class="active"><a href="#friends" aria-controls="friends" role="tab" data-toggle="tab">Friends</a></li>
                    <li role="presentation"><a href="#online" aria-controls="online" role="tab" data-toggle="tab">Online Now</a></li>
                </ul>
            
                <div class="chat-search">
                    <div class="fg-line">
                        <input type="text" class="form-control" placeholder="Search People">
                    </div>
                </div>
                </aside>
               @yield('content')
         </section>

           <footer id="footer">
            Copyright &copy; <?php echo date('Y'); ?> Profile Checker
            <ul class="f-menu">
                <li><a href="">Home</a></li>
                <li><a href="">Dashboard</a></li>
                <li><a href="">Reports</a></li>
                <li><a href="">Support</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </footer>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
    
   
      @include('Admin.footer.footer')
    </body>
  </html>
