@extends('Admin.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('Assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('Assets/jquery-confirm/jquery-confirm.css')}}">


    <section id="content" ng-controller="SettingsController" >
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                     <li class="active" style="padding-bottom: 5px"> <b>Change Username/ Password</b></li>
                                 
                                </ul>
                              
                             
                                 
                                  <div role="tabpanel" class="tab-pane active" id="scoreing"  >
                                      <div class="card">
                                     <div  class="row">
                                         
                                   <div class="col-sm-12">
                                       
                                            
                                  
        <form ng-enter="updateProfile()" name='update_profile' >
            {{csrf_field()}}
            
                        <div class="col-md-6 col-sm-6 col-xs-12 left">
          
          <div class="form-group">   
                      
                            <label for="username"><br>Username</label>
                        
                                <div class="form-line">
                                    <input type="username" id="username" required="required" name="username" class="form-control" placeholder="Username" value="{{Auth::user()->username}}" data-parsley-errors-container="#username_error">

                                </div>
                                <div id="username_error"></div>
                            </div>

                            <input type="checkbox" id="change_pass" name="change_pass" value="1" class="filled-in">
                            <label for="change_pass">Change Password</label><br /><br />
                            <label for="current_password">Current Password</label>
                            <div class="form-group">
                                <div class="form-line">
                                    <input type="password" id="current_password" required="required" name="current_password" class="form-control" placeholder="Current Password" data-parsley-errors-container="#old_pass_error">
                                </div>
                                <div id="old_pass_error"></div>
                            </div>
                            <div name="checkd-chg-pass" >
                            </div>
                            <div class="resp-msg"></div>
                            <button type="button" ng-click="updateProfile()" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                        
                   
              
        </form>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </section>

  


 <script src="/admin/jscontrols/settings_controls.js"></script>

<script src="{{asset('Assets/parsley/parsley.js')}}"></script>
<script src="{{asset('Assets/jquery-confirm/jquery-confirm.js')}}"></script>

@endsection
