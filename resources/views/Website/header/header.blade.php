<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://materialdesignthemes.com/demo/mdlwp/xmlrpc.php">

    <title>TrueCheckr </title>
   
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
        textarea {
             
                border-radius: 26px;
                border: 1px solid #d7c5c5 !important;
                    padding: 14px !important;
        }
    </style>
    <link rel='stylesheet' id='mdlwp-mdl-css-css' href='/web_assests/css/material.indigo-pink.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='mdlwp-mdl-icons-css' href='//fonts.googleapis.com/icon?family=Material+Icons&#038;ver=4.3.12' type='text/css' media='all' />
    <link rel='stylesheet' id='mdlwp-mdl-roboto-css' href='//fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C500%2C700&#038;ver=4.3.12' type='text/css' media='all' />
    <link rel='stylesheet' id='mdlwp-style-css' href='/web_assests/css/style.min.css' type='text/css' media='all' />
   <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
    <link href="/font-awesome/css/font-awesome.css" rel="stylesheet" >
        <script src="/assests/js/jquery.min.js"></script>
        <link rel="stylesheet" href="/assests/css/jquery-confirm.min.css">
        <script src="/assests/lib/angular/angular.min.js"></script>
        <script src="/assests/lib/angular/route.js"></script>      
        <script src="/assests/js/jquery-confirm.min.js"></script>
  
</head>