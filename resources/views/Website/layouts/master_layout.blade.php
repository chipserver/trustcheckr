<!DOCTYPE html>
<html lang="en-US">

    @include('Website.header.header')
<style type="text/css">
    .active_cls {
border-bottom: 10px solid white;
    }
    .disble_cls {
margin-top: -10px;
    }

</style>
<body class="home blog group-blog mdl-color-text--grey-700 mdl-base" ng-app="messageapp">
    <div id="page" class="hfeed site mdl-layout mdl-js-layout mdl-layout--fixed-header">

        <header id="masthead" class="site-header mdl-layout__header" role="banner">
            <div class="mdl-layout__header-row">
                <!-- Title -->
                <span class="mdl-layout-title">TrueCheckr</span>
                <!-- Add spacer, to align navigation to the right -->
                <div class="mdl-layout-spacer"></div>

                <div class="mdlwp-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">

                   
                </div>

               

            </div>
        </header>
         <header id="masthead" class="site-header mdl-layout__header" role="banner">
            <div class="mdl-layout__header-row">
                 
                <!-- Navigation. We hide it in small screens. -->
                <div class="mdlwp-navigation-container">
                    <nav class="mdl-navigation mdl-layout--large-screen-only" style=" float: left;">

                        <a href="/posts"   class="mdl-navigation__link menu-item menu-item-type-post_type menu-item-object-page menu-item-68 {{ Request::is('posts')   ? 'active_cls': 'disble_cls' }} " >Recent Msg</a>
                       
                        <a href="/" class="mdl-navigation__link menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-32 is-active  {{ Request::is('/') ||  Request::is('post/*') ? 'active_cls': ' disble_cls' }}"  ><span>Check</span>
                           
                        </a>
                    </nav>
                </div> 

            </div>
        </header>

       

        <main class="mdl-layout__content">
            <div id="content" class="site-content">

                <div id="primary" class="content-area">
                    <main id="main" class="site-main mdl-grid mdlwp-900" role="main">





                        <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp">
               @yield('content')
                 </div>
                        <!-- .mdl-cell -->
                     
              
                        <nav class="mdlwp-nav mdl-color-text--grey-50 mdl-cell mdl-cell--12-col" role="navigation">

                           
                           
                        </nav>
                        <!-- .navigation -->


                    </main>
                    <!-- #main -->
                </div>
                <!-- #primary -->


            </div>
            <!-- #content -->
         
      @include('Website.footer.footer')
    </body>
  </html>
