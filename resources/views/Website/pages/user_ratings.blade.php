 @extends('Admin.layouts.master_layout')
@section('content')
  <section id="content" ng-controller="MatchController">
                <div class="container">
                     <div class="card" style="padding: 10px;">
                    <div role="tabpanel">
                                <ul class="tab-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                    
                                     <li class="active"><a href="#analytics" aria-controls="analytics" role="tab" data-toggle="tab" aria-expanded="false">Api Analytics
</a></li>

                                    <li  ><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="false">Set Rating
</a></li>
                                    <li class=""><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab" aria-expanded="false">Update Rating </a></li>
                                  
                                
                                </ul>
                              
                                <div class="tab-content" style="padding: 50px;">


                                 <div role="tabpanel" class="tab-pane active" id="analytics" name="rating_cntl">
                                      <div class="card">
                                     <div  class="row">
                                   <div class="col-sm-12">
                                   <h4>Api Score</h4> 
                                    <table class="table">
                                    <thead>
                                        <tr>
                                         
                                            <th>SLNo</th>
                                            <th> Date </th>
                                            <th> Hits </th>

                                        </tr>
                                          </thead>
                                         <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>

                                        </tr>
                                    </table>
                                   
                                 </div>
                               
                        
                        
                                 </div>


                                  </div>
                                  </div>



                            <div role="tabpanel" class="tab-pane " id="home11" name="search_matching">
                             <div class="row  col-sm-offset-2">
                                      
                            <div class="col-md-8 col-sm-12">
                             <div class="col-md-4 col-sm-12"> 

                               <label style="    margin-top: 10px;">
                                   Image Type
                                  
                                </label>
                            </div>
                             <div class="col-md-4 col-sm-6">  
                               <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="image_type" value="1">
                                    <i class="input-helper"></i>Image 
                                  
                                </label>
                            </div> 
                            </div>
                                <div class="col-md-4 col-sm-6">  
                             <div class="radio m-b-15">
                                <label> 
                                    <input type="radio" name="image_type" value="2" checked="">
                                    <i class="input-helper"></i>
                                     Base64
                                </label>
                            </div>
                            </div>
                            </div>
 
                             <div class="col-md-4 col-sm-12">                     
                              &nbsp
                            </div>
                           </div>
                           <div class="row  col-sm-offset-2">
                                <div class="col-md-4 col-sm-12">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-image zmdi-hc-fw"></i></span>
                                        <div class="fg-line" name="image_input_cnt">
                                                <input type="text" class="form-control" name="image_base64" placeholder="Base64 image" required="true">
                                         </div>
                                       </div>
                                </div>
                                
                                  <div class="col-md-4 col-sm-12">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-account-box-o zmdi-hc-fw"></i></span>
                                        <div class="fg-line" >
                                                <input type="text" class="form-control" name="userId" placeholder="User ID" >
                                         </div>
                                       </div>
                                </div>
                              
                      
                                <div class="col-md-4 col-sm-12">
                                    <button class="btn btn-primary btn-icon-text waves-effect" name="set_rating" ><i class="zmdi zmdi-search"></i> Set Rating </button>
                                </div>
                                
                                     </div>

                                      <div style="display: none;    padding-bottom: 5px;" class="pre_json">
                     <h4 style="    padding-left: 20px;">  Response :</h4> 
                      <pre ><code id="json_code"> </code></pre>
                </div>
                                    </div>




                                    <div role="tabpanel" class="tab-pane " id="profile11" name="rating_cntl">
                                      <div class="card">
                                     <div  class="row">
                        <div class="col-sm-4">
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-alert-polygon zmdi-hc-fw"></i></span>
                                            <div class="dtp-container fg-line">
                                            <input type='text' class="form-control" placeholder="ImageId" name="imageid" required="true" >
                                        </div>
                                    </div>
                                </div>
<div class="col-sm-4">
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-star-half zmdi-hc-fw"></i></span>
                                            <div class="dtp-container fg-line">
                                            <input type='number' class="form-control" name="update_rating" placeholder="Rating" required="true">
                                        </div>
                                    </div>
                                </div>


                                  <div class="col-sm-4">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-account-box-o zmdi-hc-fw"></i></span>
                                        <div class="fg-line" >
                                                <input type="text" class="form-control" name="userId" placeholder="User ID" >
                                         </div>
                                       </div>
                                </div>
                                
                                <div class="col-sm-12" align="center"><button class="btn btn-primary waves-effect" name="update_ratings"><i class="zmdi zmdi-home"></i>  Update Ratings  </button>
                                </div>

                          </div><br/>
                        
                        
                    </div>


                                      <div style="display: none;    padding-bottom: 5px;" class="pre_json">
                     <h4 style="    padding-left: 20px;">  Response :</h4> 
                      <pre ><code id="json_code"> </code></pre>
                </div>
                                    </div>
                                   
                                </div>
                            </div>
                            </div>
                 
                 
                </div>
            </section>     
            <script src="/admin/jscontrols/api_controls.js"></script>
                         @endsection
