@extends('Admin.layouts.master_layout')
@section('content')
   <section id="content"  ng-controller="MatchController">
                <div class="container">
                   <div class="card">
                         <div role="tabpanel">
                          <div class="card-body card-padding">
                            <div class="card-body card-padding" style="padding-bottom: 0px;">
                             <ul class="tab-nav text-center fw-nav" role="tablist" tabindex="1" style="overflow: hidden; outline: none;">
                                    <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" aria-expanded="true"> Match User</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home11"  name="search_matching">
                                       <div class="row  col-sm-offset-2">
                                      
                            <div class="col-md-8 col-sm-12">
                             <div class="col-md-4 col-sm-12"> 

                               <label style="    margin-top: 10px;">
                                   Image Type
                                  
                                </label>
                            </div>
                             <div class="col-md-4 col-sm-6">  
                               <div class="radio m-b-15">
                                <label>
                                    <input type="radio" name="image_type" value="1">
                                    <i class="input-helper"></i>Image 
                                  
                                </label>
                            </div> 
                            </div>
                                <div class="col-md-4 col-sm-6">  
                             <div class="radio m-b-15">
                                <label> 
                                    <input type="radio" name="image_type" value="2" checked="">
                                    <i class="input-helper"></i>
                                     Base64
                                </label>
                            </div>
                            </div>
                            </div>
 
                             <div class="col-md-4 col-sm-12">                     
                              &nbsp
                            </div>
                       </div>
                           <div class="row  col-sm-offset-2">
                                <div class="col-md-4 col-sm-12">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-image zmdi-hc-fw"></i></span>
                                        <div class="fg-line" name="image_input_cnt">
                                                <input type="text" class="form-control" name="image_base64" placeholder="Base64 image" required="true">
                                         </div>
                                       </div>
                                </div>
                                 <div class="col-md-4 col-sm-12">                       
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-key zmdi-hc-fw"></i></span>
                                        <div class="fg-line">
                                                <input type="text" class="form-control" name="fb_token"  placeholder="Token" required="true">
                                         </div>
                                       </div>
                                     
                                 </div>
                                
                              
                      
                                <div class="col-md-4 col-sm-12">
                                    <button class="btn btn-primary btn-icon-text waves-effect searchuser_btn" name="match_user" ><i class="zmdi zmdi-search"></i> Search</button>
                                </div>
                                
                            </div>
                                    </div>
                                  
                                    
                                 
                            </div>

                             <br><br>
                        <p ng-if="searchString"> OCR String : <a href="javascript:void(0)"  style="word-break:break-all" target="_blank" ng-cloak >@{{searchString}} </a></p>
                           
                        <p ng-if="searching_url"> Facebook Graph url : <a href="@{{ searching_url }}"  style="word-break:break-all" target="_blank" ng-cloak class="fb-url-disp">@{{searching_url}} </a></p>
                        
                       
                        
                         <br><br>


                            <div class="row col-sm-offset-2">
                        <div class="listview" style="    margin-left: 20%;
    margin-right: 20%;">
                              
                                <div class="lv-body" ng-repeat="user in displayuser"  ng-cloak>
                                
                                        <div class="media">
                                            <div class="pull-left">
                                             <a href="@{{user.picture.data.url}}" target="_blank">   <img class="lv-img-sm" src="@{{user.picture.data.url}}" alt=""></a>
                                            </div>
                                            <div class="media-body" >
                                                <div class="lv-title">  @{{ user.name}} </div>
                                              
                                                 <small class="lv-small">
                                                  <span class="head-info"> Facebook ID : </span>
                                                  <span  >
                                                <a href="https://facebook.com/@{{user.id }}" target="_blank">@{{  user.id}}  </a>
                                                  </span>
                                                  </small>
                                                  <br>
                                                   <small class="lv-small" >
                                                  <span class="head-info"> Similarity : </span>
                                                  <span >
                                                <a href="https://facebook.com/@{{user.id }}" target="_blank">@{{ user.Similarity }}  </a>
                                                  </span>
                                                  </small>
                                                  
                                           </div>
                                        </div>
                                
                                 </div> 
                 
                             
                            </div>

                       

                           <br><br>
                        <p ng-if="ocr_resp"> OCR : <a href="javascript:void(0)"   style="word-break:break-all" target="_blank" ng-cloak >@{{ocr_resp}} </a></p>

                        
                        </div>
                        
                      
                        </div>
                       
                    </div>
                
                    
                </div>
             
               
             
             
             
            </section>

        
     @endsection
