<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Web\Admin'], function () {
Route::group(['middleware' => ['guest']], function () {
Route::get('/', 'WebPageControl@index');
Route::get('/posts', 'WebPageControl@posts');
Route::get('/post/{id}', 'WebPageControl@post');

});
Route::group(['prefix' => 'my-account'], function () {
	Route::group(['middleware' => ['guest']], function (){
	  Route::get('/', 'AdminControl@index');
	});
	
  Route::group(['middleware' =>'auth'], function() {
	Route::get('/dashboard',  'DashboardControl@index');
	Route::get('/settings','AdminControl@get_settings');
	Route::get('/analytics/{id}','ApiControl@api_anlaytic');
  });
});

Route::group(['prefix' => 'superadmin'], function () {
Route::get('/logout', 'AdminControl@signout_func');
Route::group(['middleware' => ['guest']], function (){
Route::get('/', 'AdminControl@index');
Route::post('/auth-login', 'AdminControl@authlogin_func');
});

Route::group(['middleware' =>'auth'], function() {
	Route::get('/trustscrore-checker','ApiControl@trustscrore_checker');
Route::get('/dashboard',  'DashboardControl@index');
Route::get('/apis-keys','ApiControl@api_keys');
Route::get('/pan-voter-dl-keys','ApiControl@pan_voter_dl');

Route::get('/userdetails','AdminControl@get_user_details');
Route::get('/user-details','AdminControl@user_details');

  Route::post('/user', 'AdminControl@user_func');
  
   Route::post('/delete', 'AdminControl@delete_func');
   Route::get('/edituser/{id}', 'AdminControl@edituser_details');
   Route::post('/updateuser/{id}', 'AdminControl@update_func');

Route::get('/add-apis-keys','ApiControl@add_api_keys');
Route::post('/post-apis-keys','ApiControl@post_apis_keys');

Route::get('/analytics/{id}','ApiControl@api_anlaytic');

Route::get('/settings','AdminControl@get_settings');

Route::post('/sub-settings','AdminControl@change_settings');

Route::get('/export-analytics/{name}/{id}','ApiControl@export_api_anlaytic');

Route::post('/credits',  'ApiControl@post_credits');

Route::get('/history-credits/{id}',  'ApiControl@get_history_credits');

Route::get('/display-analytics/{id}','ApiControl@display_api_anlaytic');
Route::get('/get-analytics/{id}','ApiControl@get_api_anlaytic');
Route::get('/import-export', 'ApiControl@import_export');	
Route::post('/import','ApiControl@import_upload');
Route::get('/export/{id}','ApiControl@export');
Route::get('/gender-upload','ApiControl@display_genderupload');
Route::post('/export-gender-check','ApiControl@export_gender_check');



Route::get('/bulkexport-download/{id}','ApiControl@buk_export_download');
Route::get('/bulkexport','ApiControl@bulk_export1');
Route::post('/importexcel-file','ApiControl@importexcel_file_upload');


Route::get('/bulk-phone-upload' ,'BulkImportControl@load_phone_upload'); 
Route::post('/bulk-phone-upload' ,'BulkImportControl@phone_upload');
Route::get('/bulk-phone-get/{id}','BulkImportControl@phone_upload_byid');
Route::post('/bulk-email-upload','BulkImportControl@email_upload');
Route::get('/bulk-email-get/{id}','BulkImportControl@email_upload_byid');
Route::get('/bulk-email-upload' ,'BulkImportControl@load_email_upload');



});




});
});
